package cmd

const (
	DenomRegex                  = `[a-zA-Z][a-zA-Z0-9:\\/\\\-\\_\\.]{2,127}`
	DDChainCoinType    uint32 = 931
	DDChainCoinPurpose uint32 = 44
	DDChainHDPath      string = `m/44'/931'/0'/0/0`
)
