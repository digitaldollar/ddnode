//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package cmd

const (
	Bech32PrefixAccAddr  = "dd"
	Bech32PrefixAccPub   = "ddpub"
	Bech32PrefixValAddr  = "ddv"
	Bech32PrefixValPub   = "ddvpub"
	Bech32PrefixConsAddr = "ddc"
	Bech32PrefixConsPub  = "ddcpub"
)
