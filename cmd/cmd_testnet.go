//go:build testnet || mocknet
// +build testnet mocknet

package cmd

const (
	Bech32PrefixAccAddr  = "dd"
	Bech32PrefixAccPub   = "ddpub"
	Bech32PrefixValAddr  = "ddv"
	Bech32PrefixValPub   = "ddvpub"
	Bech32PrefixConsAddr = "ddc"
	Bech32PrefixConsPub  = "ddcpub"
)
