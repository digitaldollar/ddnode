package main

import (
	"flag"
	"fmt"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

func main() {
	raw := flag.String("p", "", "dd bech32 pubkey")
	flag.Parse()

	if len(*raw) == 0 {
		panic("no pubkey provided")
	}

	// Read in the configuration file for the sdk
	nw := common.CurrentChainNetwork
	switch nw {
	case common.TestNet:
		fmt.Println("DDChain testnet:")
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("dd", "ddpub")
		config.SetBech32PrefixForValidator("ddv", "ddvpub")
		config.SetBech32PrefixForConsensusNode("ddc", "ddcpub")
		config.Seal()
	case common.MainNet:
		fmt.Println("DDChain mainnet:")
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("dd", "ddpub")
		config.SetBech32PrefixForValidator("ddv", "ddvpub")
		config.SetBech32PrefixForConsensusNode("ddc", "ddcpub")
		config.Seal()
	}

	pk, err := common.NewPubKey(*raw)
	if err != nil {
		panic(err)
	}

	chains := common.Chains{
		common.DDChain,
		common.BNBChain,
		common.BTCChain,
		common.ETHChain,
	}

	for _, chain := range chains {
		addr, err := pk.GetAddress(chain)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s Address: %s\n", chain.String(), addr)
	}
}
