package gaia

import (
	ctypes "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
	. "gopkg.in/check.v1"
)

type UtilTestSuite struct{}

var _ = Suite(&UtilTestSuite{})

func (s *UtilTestSuite) SetUpSuite(c *C) {}

func (s *UtilTestSuite) TestFromCosmosToDdchain(c *C) {
	// 5 ATOM, 6 decimals
	cosmosCoin := cosmos.NewCoin("uatom", ctypes.NewInt(5000000))
	ddchainCoin, err := fromCosmosToDdchain(cosmosCoin)
	c.Assert(err, IsNil)

	// 5 ATOM, 8 decimals
	expectedDdchainAsset, err := common.NewAsset("GAIA.ATOM")
	c.Assert(err, IsNil)
	expectedDdchainAmount := ctypes.NewUint(500000000)
	c.Check(ddchainCoin.Asset.Equals(expectedDdchainAsset), Equals, true)
	c.Check(ddchainCoin.Amount.BigInt().Int64(), Equals, expectedDdchainAmount.BigInt().Int64())
	c.Check(ddchainCoin.Decimals, Equals, int64(6))
}

func (s *UtilTestSuite) TestFromDdchainToCosmos(c *C) {
	// 6 GAIA.ATOM, 8 decimals
	ddchainAsset, err := common.NewAsset("GAIA.ATOM")
	c.Assert(err, IsNil)
	ddchainCoin := common.Coin{
		Asset:    ddchainAsset,
		Amount:   cosmos.NewUint(600000000),
		Decimals: 6,
	}
	cosmosCoin, err := fromDdchainToCosmos(ddchainCoin)
	c.Assert(err, IsNil)

	// 6 uatom, 6 decimals
	expectedCosmosDenom := "uatom"
	expectedCosmosAmount := int64(6000000)
	c.Check(cosmosCoin.Denom, Equals, expectedCosmosDenom)
	c.Check(cosmosCoin.Amount.Int64(), Equals, expectedCosmosAmount)
}
