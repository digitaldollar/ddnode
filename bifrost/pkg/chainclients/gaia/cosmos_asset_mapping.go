package gaia

import "strings"

type CosmosAssetMapping struct {
	CosmosDenom     string
	CosmosDecimals  int
	DDChainSymbol string
}

// CosmosAssetMappings maps a Cosmos denom to a DDChain symbol and provides the asset decimals
// CHANGEME: define assets that should be observed by DDChain here. This also acts a whitelist.
var CosmosAssetMappings = []CosmosAssetMapping{
	{
		CosmosDenom:     "uatom",
		CosmosDecimals:  6,
		DDChainSymbol: "ATOM",
	},
}

func GetAssetByCosmosDenom(denom string) (CosmosAssetMapping, bool) {
	for _, asset := range CosmosAssetMappings {
		if strings.EqualFold(asset.CosmosDenom, denom) {
			return asset, true
		}
	}
	return CosmosAssetMapping{}, false
}

func GetAssetByDdchainSymbol(symbol string) (CosmosAssetMapping, bool) {
	for _, asset := range CosmosAssetMappings {
		if strings.EqualFold(asset.DDChainSymbol, symbol) {
			return asset, true
		}
	}
	return CosmosAssetMapping{}, false
}
