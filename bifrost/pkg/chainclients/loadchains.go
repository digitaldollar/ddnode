package chainclients

import (
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/digitaldollar/tss/go-tss/tss"

	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/dogecoin"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/evm"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/gaia"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/utxo"

	"gitlab.com/digitaldollar/ddnode/bifrost/metrics"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/binance"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/bitcoin"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/bitcoincash"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/ethereum"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/litecoin"
	"gitlab.com/digitaldollar/ddnode/bifrost/pkg/chainclients/shared/types"
	"gitlab.com/digitaldollar/ddnode/bifrost/pubkeymanager"
	"gitlab.com/digitaldollar/ddnode/bifrost/ddclient"
	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/config"
)

// ChainClient exports the shared type.
type ChainClient = types.ChainClient

// LoadChains returns chain clients from chain configuration
func LoadChains(ddKeys *ddclient.Keys,
	cfg map[common.Chain]config.BifrostChainConfiguration,
	server *tss.TssServer,
	ddchainBridge ddclient.DdchainBridge,
	m *metrics.Metrics,
	pubKeyValidator pubkeymanager.PubKeyValidator,
	poolMgr ddclient.PoolManager,
) (chains map[common.Chain]ChainClient, restart chan struct{}) {
	logger := log.Logger.With().Str("module", "bifrost").Logger()

	chains = make(map[common.Chain]ChainClient)
	restart = make(chan struct{})
	failedChains := []common.Chain{}

	loadChain := func(chain config.BifrostChainConfiguration) (ChainClient, error) {
		switch chain.ChainID {
		case common.BNBChain:
			return binance.NewBinance(ddKeys, chain, server, ddchainBridge, m)
		case common.ETHChain:
			return ethereum.NewClient(ddKeys, chain, server, ddchainBridge, m, pubKeyValidator, poolMgr)
		case common.AVAXChain, common.BSCChain:
			return evm.NewEVMClient(ddKeys, chain, server, ddchainBridge, m, pubKeyValidator, poolMgr)
		case common.GAIAChain:
			return gaia.NewCosmosClient(ddKeys, chain, server, ddchainBridge, m)
		case common.BTCChain:
			return bitcoin.NewClient(ddKeys, chain, server, ddchainBridge, m)
		case common.BCHChain:
			return bitcoincash.NewClient(ddKeys, chain, server, ddchainBridge, m)
		case common.LTCChain:
			return litecoin.NewClient(ddKeys, chain, server, ddchainBridge, m)
		case common.DOGEChain:
			if chain.UTXO.ClientV2 {
				return utxo.NewClient(ddKeys, chain, server, ddchainBridge, m)
			} else {
				return dogecoin.NewClient(ddKeys, chain, server, ddchainBridge, m)
			}
		default:
			log.Fatal().Msgf("chain %s is not supported", chain.ChainID)
			return nil, nil
		}
	}

	for _, chain := range cfg {
		if chain.Disabled {
			logger.Info().Msgf("%s chain is disabled by configure", chain.ChainID)
			continue
		}

		client, err := loadChain(chain)
		if err != nil {
			logger.Error().Err(err).Stringer("chain", chain.ChainID).Msg("failed to load chain")
			failedChains = append(failedChains, chain.ChainID)
			continue
		}

		// trunk-ignore-all(golangci-lint/forcetypeassert)
		if chain.UTXO.ClientV2 {
			pubKeyValidator.RegisterCallback(client.(*utxo.Client).RegisterPublicKey)
		} else {
			switch chain.ChainID {
			case common.BTCChain:
				pubKeyValidator.RegisterCallback(client.(*bitcoin.Client).RegisterPublicKey)
			case common.BCHChain:
				pubKeyValidator.RegisterCallback(client.(*bitcoincash.Client).RegisterPublicKey)
			case common.LTCChain:
				pubKeyValidator.RegisterCallback(client.(*litecoin.Client).RegisterPublicKey)
			case common.DOGEChain:
				pubKeyValidator.RegisterCallback(client.(*dogecoin.Client).RegisterPublicKey)
			}
		}
		chains[chain.ChainID] = client
	}

	// watch failed chains and restart bifrost if any succeed init
	if len(failedChains) > 0 {
		go func() {
			tick := time.NewTicker(config.GetBifrost().BackOff.MaxInterval)
			for range tick.C {
				for _, chain := range failedChains {
					ccfg := cfg[chain]
					ccfg.BlockScanner.DBPath = "" // in-memory db

					_, err := loadChain(ccfg)
					if err == nil {
						logger.Info().Stringer("chain", chain).Msg("chain loaded, restarting bifrost")
						close(restart)
						return
					} else {
						logger.Error().Err(err).Stringer("chain", chain).Msg("failed to load chain")
					}
				}
			}
		}()
	}

	return chains, restart
}
