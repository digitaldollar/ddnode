package signer

import (
	"fmt"
	"sync"

	"gitlab.com/digitaldollar/ddnode/bifrost/ddclient"
	"gitlab.com/digitaldollar/ddnode/constants"
)

// ConstantsProvider which will query ddchain to get the constants value per request
// it will also cache the constant values internally
type ConstantsProvider struct {
	requestHeight int64 // the block height last request to ddchain to retrieve constant values
	bridge        ddclient.DdchainBridge
	constantsLock *sync.Mutex
	constants     map[string]int64 // the constant values get from ddchain and cached in memory
}

// NewConstantsProvider create a new instance of ConstantsProvider
func NewConstantsProvider(bridge ddclient.DdchainBridge) *ConstantsProvider {
	return &ConstantsProvider{
		constants:     make(map[string]int64),
		requestHeight: 0,
		bridge:        bridge,
		constantsLock: &sync.Mutex{},
	}
}

// GetInt64Value get the constant value that match the given key
func (cp *ConstantsProvider) GetInt64Value(ddchainBlockHeight int64, key constants.ConstantName) (int64, error) {
	if err := cp.EnsureConstants(ddchainBlockHeight); err != nil {
		return 0, fmt.Errorf("fail to get constants from ddchain: %w", err)
	}
	cp.constantsLock.Lock()
	defer cp.constantsLock.Unlock()
	return cp.constants[key.String()], nil
}

func (cp *ConstantsProvider) EnsureConstants(ddchainBlockHeight int64) error {
	if cp.requestHeight == 0 {
		return cp.getConstantsFromDdchain(ddchainBlockHeight)
	}
	cp.constantsLock.Lock()
	churnInterval := cp.constants[constants.ChurnInterval.String()]
	cp.constantsLock.Unlock()
	// Ddchain will have new version and constants only when new node get rotated in , and the new version get consensus
	if ddchainBlockHeight-cp.requestHeight < churnInterval {
		return nil
	}
	return cp.getConstantsFromDdchain(ddchainBlockHeight)
}

func (cp *ConstantsProvider) getConstantsFromDdchain(height int64) error {
	constants, err := cp.bridge.GetConstants()
	if err != nil {
		return fmt.Errorf("fail to get constants: %w", err)
	}
	cp.constantsLock.Lock()
	defer cp.constantsLock.Unlock()
	cp.constants = constants
	cp.requestHeight = height
	return nil
}
