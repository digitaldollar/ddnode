package signer

import (
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
	"time"

	. "gopkg.in/check.v1"

	"github.com/cosmos/cosmos-sdk/crypto/hd"
	cKeys "github.com/cosmos/cosmos-sdk/crypto/keyring"
	ctypes "gitlab.com/digitaldollar/binance-sdk/common/types"

	"gitlab.com/digitaldollar/ddnode/bifrost/metrics"
	"gitlab.com/digitaldollar/ddnode/bifrost/pubkeymanager"
	"gitlab.com/digitaldollar/ddnode/bifrost/ddclient"
	"gitlab.com/digitaldollar/ddnode/cmd"
	"gitlab.com/digitaldollar/ddnode/config"
	"gitlab.com/digitaldollar/ddnode/x/ddchain"
	types2 "gitlab.com/digitaldollar/ddnode/x/ddchain/types"
)

func Test(t *testing.T) { TestingT(t) }

type DdchainBlockScanSuite struct {
	dddir  string
	ddKeys *ddclient.Keys
	bridge   ddclient.DdchainBridge
	m        *metrics.Metrics
	storage  *SignerStore
	rpcHost  string
}

var _ = Suite(&DdchainBlockScanSuite{})

func (s *DdchainBlockScanSuite) SetUpSuite(c *C) {
	ddchain.SetupConfigForTest()
	s.m = GetMetricForTest(c)
	c.Assert(s.m, NotNil)
	ns := strconv.Itoa(time.Now().Nanosecond())
	types2.SetupConfigForTest()
	ctypes.Network = ctypes.TestNetwork
	c.Assert(os.Setenv("NET", "testnet"), IsNil)

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		c.Logf("requestUri:%s", req.RequestURI)
		if strings.HasPrefix(req.RequestURI, "/txs") { // nolint
			_, err := rw.Write([]byte(`{ "jsonrpc": "2.0", "id": "", "result": { "height": "1", "txhash": "ENULZOBGZHEKFOIBYRLLBELKFZVGXOBLTRQGTOWNDHMPZQMBLGJETOXJLHPVQIKY", "logs": [{"success": "true", "log": ""}] } }`))
			c.Assert(err, IsNil)
		} else if strings.HasPrefix(req.RequestURI, "/ddchain/lastblock/BNB") {
			_, err := rw.Write([]byte(`{ "jsonrpc": "2.0", "id": "", "result": { "chain": "BNB", "lastobservedin": "1", "lastsignedout": "1", "ddchain": "1" } }`))
			c.Assert(err, IsNil)
		} else if strings.HasPrefix(req.RequestURI, "/ddchain/lastblock") {
			_, err := rw.Write([]byte(`{ "jsonrpc": "2.0", "id": "", "result": { "chain": "DdChain", "lastobservedin": "1", "lastsignedout": "1", "ddchain": "1" } }`))
			c.Assert(err, IsNil)
		} else if strings.HasPrefix(req.RequestURI, "/auth/accounts/") {
			_, err := rw.Write([]byte(`{ "jsonrpc": "2.0", "id": "", "result": { "height": "1", "result": { "value": { "account_number": "0", "sequence": "0" } } } |`))
			c.Assert(err, IsNil)
		} else if strings.HasPrefix(req.RequestURI, "/ddchain/vaults/pubkeys") {
			_, err := rw.Write([]byte(`{ "jsonrpc": "2.0", "id": "", "result": { "asgard": ["ddpub1addwnpepqfgfxharps79pqv8fv9ndqh90smw8c3slrtrssn58ryc5g3p9sx856x07yn"], "yggdrasil": ["ddpub1addwnpepqdqvd4r84lq9m54m5kk9sf4k6kdgavvch723pcgadulxd6ey9u70k6zq8qe"] } }`))
			c.Assert(err, IsNil)
		} else if req.RequestURI == "/block" {
			_, err := rw.Write([]byte(`{ "jsonrpc": "2.0", "id": "", "result": { "block": { "header": { "height": "1" } } } }`))
			c.Assert(err, IsNil)
		} else if strings.HasPrefix(req.RequestURI, "/ddchain/keysign") {
			_, err := rw.Write([]byte(`{
			"chains": {
				"BNB": {
					"chain": "BNB",
					"hash": "",
					"height": "1",
					"tx_array": [
						{
							"chain": "BNB",
							"coin": {
								"amount": "10000000000",
								"asset": "BNB.BNB"
							},
							"in_hash": "ENULZOBGZHEKFOIBYRLLBELKFZVGXOBLTRQGTOWNDHMPZQMBLGJETOXJLHPVQIKY",
							"memo": "",
							"out_hash": "",
							"to": "bnb145wcuncewfkuc4v6an0r9laswejygcul43c3wu",
							"vault_pubkey": "ddpub1addwnpepqfgfxharps79pqv8fv9ndqh90smw8c3slrtrssn58ryc5g3p9sx856x07yn"
						}
					]
				}
			}
		}
	`))
			c.Assert(err, IsNil)
		} else if strings.HasPrefix(req.RequestURI, "/ddchain/keygen") {
			_, err := rw.Write([]byte(`{
		"height": "1",
		"keygens": [
		{
			"id": "AAAA000000000000000000000000000000000000000000000000000000000000",
			"type": "asgard",
			"members": [
				"ddpub1addwnpepqfgfxharps79pqv8fv9ndqh90smw8c3slrtrssn58ryc5g3p9sx856x07yn"
			]
		}]}`))
			c.Assert(err, IsNil)
		} else if strings.HasSuffix(req.RequestURI, "/signers") {
			_, err := rw.Write([]byte(`[
				"ddpub1addwnpepqfgfxharps79pqv8fv9ndqh90smw8c3slrtrssn58ryc5g3p9sx856x07yn"
		]`))
			c.Assert(err, IsNil)
		}
	}))

	s.dddir = filepath.Join(os.TempDir(), ns, ".ddcli")
	splitted := strings.SplitAfter(server.URL, ":")
	s.rpcHost = splitted[len(splitted)-1]
	cfg := config.BifrostClientConfiguration{
		ChainID:         "ddchain",
		ChainHost:       "localhost:" + s.rpcHost,
		SignerName:      "bob",
		SignerPasswd:    "password",
		ChainHomeFolder: s.dddir,
	}

	kb := cKeys.NewInMemory()
	_, _, err := kb.NewMnemonic(cfg.SignerName, cKeys.English, cmd.DDChainHDPath, cfg.SignerPasswd, hd.Secp256k1)
	c.Assert(err, IsNil)
	s.ddKeys = ddclient.NewKeysWithKeybase(kb, cfg.SignerName, cfg.SignerPasswd)
	c.Assert(err, IsNil)
	s.bridge, err = ddclient.NewDdchainBridge(cfg, s.m, s.ddKeys)
	c.Assert(err, IsNil)
	s.storage, err = NewSignerStore("signer_data", config.LevelDBOptions{}, "")
	c.Assert(err, IsNil)
}

func (s *DdchainBlockScanSuite) TearDownSuite(c *C) {
	c.Assert(os.Unsetenv("NET"), IsNil)

	if err := os.RemoveAll(s.dddir); err != nil {
		c.Error(err)
	}

	if err := os.RemoveAll("signer_data"); err != nil {
		c.Error(err)
	}
	tempPath := filepath.Join(os.TempDir(), "/var/data/bifrost/signer")
	if err := os.RemoveAll(tempPath); err != nil {
		c.Error(err)
	}

	if err := os.RemoveAll("signer/var"); err != nil {
		c.Error(err)
	}
}

func (s *DdchainBlockScanSuite) TestProcess(c *C) {
	cfg := config.BifrostBlockScannerConfiguration{
		RPCHost:                    "127.0.0.1:" + s.rpcHost,
		ChainID:                    "DdChain",
		StartBlockHeight:           1,
		EnforceBlockHeight:         true,
		BlockScanProcessors:        1,
		BlockHeightDiscoverBackoff: time.Second,
		BlockRetryInterval:         10 * time.Second,
	}
	blockScan, err := NewDdchainBlockScan(cfg, s.storage, s.bridge, s.m, pubkeymanager.NewMockPoolAddressValidator())
	c.Assert(blockScan, NotNil)
	c.Assert(err, IsNil)
}
