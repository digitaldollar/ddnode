package signer

import (
	"errors"
	"fmt"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/digitaldollar/ddnode/bifrost/blockscanner"
	btypes "gitlab.com/digitaldollar/ddnode/bifrost/blockscanner/types"
	"gitlab.com/digitaldollar/ddnode/bifrost/metrics"
	"gitlab.com/digitaldollar/ddnode/bifrost/pubkeymanager"
	"gitlab.com/digitaldollar/ddnode/bifrost/ddclient"
	"gitlab.com/digitaldollar/ddnode/bifrost/ddclient/types"
	"gitlab.com/digitaldollar/ddnode/config"
	ttypes "gitlab.com/digitaldollar/ddnode/x/ddchain/types"
)

type DdchainBlockScan struct {
	logger         zerolog.Logger
	wg             *sync.WaitGroup
	stopChan       chan struct{}
	txOutChan      chan types.TxOut
	keygenChan     chan ttypes.KeygenBlock
	cfg            config.BifrostBlockScannerConfiguration
	scannerStorage blockscanner.ScannerStorage
	ddchain      ddclient.DdchainBridge
	errCounter     *prometheus.CounterVec
	pubkeyMgr      pubkeymanager.PubKeyValidator
}

// NewDdchainBlockScan create a new instance of ddchain block scanner
func NewDdchainBlockScan(cfg config.BifrostBlockScannerConfiguration, scanStorage blockscanner.ScannerStorage, ddchain ddclient.DdchainBridge, m *metrics.Metrics, pubkeyMgr pubkeymanager.PubKeyValidator) (*DdchainBlockScan, error) {
	if scanStorage == nil {
		return nil, errors.New("scanStorage is nil")
	}
	if m == nil {
		return nil, errors.New("metric is nil")
	}
	return &DdchainBlockScan{
		logger:         log.With().Str("module", "blockscanner").Str("chain", "DD").Logger(),
		wg:             &sync.WaitGroup{},
		stopChan:       make(chan struct{}),
		txOutChan:      make(chan types.TxOut),
		keygenChan:     make(chan ttypes.KeygenBlock),
		cfg:            cfg,
		scannerStorage: scanStorage,
		ddchain:      ddchain,
		errCounter:     m.GetCounterVec(metrics.DdchainBlockScannerError),
		pubkeyMgr:      pubkeyMgr,
	}, nil
}

// GetMessages return the channel
func (b *DdchainBlockScan) GetTxOutMessages() <-chan types.TxOut {
	return b.txOutChan
}

func (b *DdchainBlockScan) GetKeygenMessages() <-chan ttypes.KeygenBlock {
	return b.keygenChan
}

func (b *DdchainBlockScan) GetHeight() (int64, error) {
	return b.ddchain.GetBlockHeight()
}

func (c *DdchainBlockScan) FetchMemPool(height int64) (types.TxIn, error) {
	return types.TxIn{}, nil
}

func (b *DdchainBlockScan) FetchTxs(height, _ int64) (types.TxIn, error) {
	if err := b.processTxOutBlock(height); err != nil {
		return types.TxIn{}, err
	}
	if err := b.processKeygenBlock(height); err != nil {
		return types.TxIn{}, err
	}
	return types.TxIn{}, nil
}

func (b *DdchainBlockScan) processKeygenBlock(blockHeight int64) error {
	pk := b.pubkeyMgr.GetNodePubKey()
	keygen, err := b.ddchain.GetKeygenBlock(blockHeight, pk.String())
	if err != nil {
		return fmt.Errorf("fail to get keygen from ddchain: %w", err)
	}

	// custom error (to be dropped and not logged) because the block is
	// available yet
	if keygen.Height == 0 {
		return btypes.ErrUnavailableBlock
	}

	if len(keygen.Keygens) > 0 {
		b.keygenChan <- keygen
	}
	return nil
}

func (b *DdchainBlockScan) processTxOutBlock(blockHeight int64) error {
	for _, pk := range b.pubkeyMgr.GetSignPubKeys() {
		if len(pk.String()) == 0 {
			continue
		}
		tx, err := b.ddchain.GetKeysign(blockHeight, pk.String())
		if err != nil {
			if errors.Is(err, btypes.ErrUnavailableBlock) {
				// custom error (to be dropped and not logged) because the block is
				// available yet
				return btypes.ErrUnavailableBlock
			}
			return fmt.Errorf("fail to get keysign from block scanner: %w", err)
		}

		if len(tx.TxArray) == 0 {
			b.logger.Debug().Int64("block", blockHeight).Msg("nothing to process")
			continue
		}
		b.txOutChan <- tx
	}
	return nil
}
