package types

import (
	"gitlab.com/digitaldollar/ddnode/common"
)

// Solvency structure is to hold all the information necessary to report solvency to DDNode
type Solvency struct {
	Height int64
	Chain  common.Chain
	PubKey common.PubKey
	Coins  common.Coins
}
