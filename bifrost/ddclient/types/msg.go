package types

import (
	stypes "gitlab.com/digitaldollar/ddnode/x/ddchain/types"
)

type Msg struct {
	Type  string                 `json:"type"`
	Value stypes.MsgObservedTxIn `json:"value"`
}
