package ddclient

import (
	"encoding/json"
	"fmt"

	"gitlab.com/digitaldollar/ddnode/x/ddchain/types"
)

// GetNodeAccount retrieves node account for this address from ddchain
func (b *ddchainBridge) GetNodeAccount(ddAddr string) (*types.NodeAccount, error) {
	path := fmt.Sprintf("%s/%s", NodeAccountEndpoint, ddAddr)
	fmt.Println("-----------Node Account Path----------------------")
	fmt.Println(ddAddr)
	fmt.Println(path)
	body, _, err := b.getWithPath(path)
	if err != nil {
		return &types.NodeAccount{}, fmt.Errorf("failed to get node account: %w", err)
	}
	var na types.NodeAccount
	if err := json.Unmarshal(body, &na); err != nil {
		return &types.NodeAccount{}, fmt.Errorf("failed to unmarshal node account: %w", err)
	}
	fmt.Println(na)
	fmt.Println("-----------Node Account Path------------")
	return &na, nil
}
