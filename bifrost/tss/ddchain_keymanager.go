package tss

import (
	"gitlab.com/digitaldollar/binance-sdk/keys"
	"gitlab.com/digitaldollar/binance-sdk/types/tx"

	"gitlab.com/digitaldollar/ddnode/common"
)

// DdchainKeyManager it is a composite of binance chain keymanager
type DdchainKeyManager interface {
	keys.KeyManager
	SignWithPool(msg tx.StdSignMsg, poolPubKey common.PubKey) ([]byte, error)
	RemoteSign(msg []byte, poolPubKey string) ([]byte, []byte, error)
	Start()
	Stop()
}
