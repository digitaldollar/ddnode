package ddchain

import (
	"github.com/blang/semver"
	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

type ManageDDNameMemo struct {
	MemoBase
	Name           string
	Chain          common.Chain
	Address        common.Address
	PreferredAsset common.Asset
	Expire         int64
	Owner          cosmos.AccAddress
}

func (m ManageDDNameMemo) GetName() string            { return m.Name }
func (m ManageDDNameMemo) GetChain() common.Chain     { return m.Chain }
func (m ManageDDNameMemo) GetAddress() common.Address { return m.Address }
func (m ManageDDNameMemo) GetBlockExpire() int64      { return m.Expire }

func NewManageDDNameMemo(name string, chain common.Chain, addr common.Address, expire int64, asset common.Asset, owner cosmos.AccAddress) ManageDDNameMemo {
	return ManageDDNameMemo{
		MemoBase:       MemoBase{TxType: TxDDName},
		Name:           name,
		Chain:          chain,
		Address:        addr,
		PreferredAsset: asset,
		Expire:         expire,
		Owner:          owner,
	}
}

func (p *parser) ParseManageDDNameMemo() (ManageDDNameMemo, error) {
	switch {
	case p.version.GTE(semver.MustParse("1.116.0")):
		return p.ParseManageDDNameMemoV116()
	default:
		return ParseManageDDNameMemoV1(p.parts)
	}
}

func (p *parser) ParseManageDDNameMemoV116() (ManageDDNameMemo, error) {
	chain := p.getChain(2, true, common.EmptyChain)
	addr := p.getAddress(3, true, common.NoAddress)
	owner := p.getAccAddress(4, false, nil)
	preferredAsset := p.getAsset(5, false, common.EmptyAsset)
	expire := p.getInt64(6, false, 0)
	return NewManageDDNameMemo(p.get(1), chain, addr, expire, preferredAsset, owner), p.Error()
}
