package ddchain

import "gitlab.com/digitaldollar/ddnode/common"

func ParseDonateMemoV1(asset common.Asset) (DonateMemo, error) {
	return DonateMemo{
		MemoBase: MemoBase{TxType: TxDonate, Asset: asset},
	}, nil
}
