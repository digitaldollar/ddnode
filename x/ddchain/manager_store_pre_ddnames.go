package ddchain

import (
	"encoding/json"
	"fmt"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

type PreRegisterDDName struct {
	Name    string
	Address string
}

func getPreRegisterDDNames(ctx cosmos.Context, blockheight int64) ([]DDName, error) {
	var register []PreRegisterDDName
	if err := json.Unmarshal(preregisterDDNames, &register); err != nil {
		return nil, fmt.Errorf("fail to load preregistation ddname list,err: %w", err)
	}

	names := make([]DDName, 0)
	for _, reg := range register {
		addr, err := common.NewAddress(reg.Address)
		if err != nil {
			ctx.Logger().Error("fail to parse address", "address", reg.Address, "error", err)
			continue
		}
		name := NewDDName(reg.Name, blockheight, []DDNameAlias{{Chain: common.DDChain, Address: addr}})
		acc, err := cosmos.AccAddressFromBech32(reg.Address)
		if err != nil {
			ctx.Logger().Error("fail to parse acc address", "address", reg.Address, "error", err)
			continue
		}
		name.Owner = acc
		names = append(names, name)
	}
	return names, nil
}
