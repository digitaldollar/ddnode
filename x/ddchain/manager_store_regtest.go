//go:build regtest
// +build regtest

package ddchain

import (
	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

func migrateStoreV86(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV88(ctx cosmos.Context, mgr Manager) {}

func migrateStoreV102(ctx cosmos.Context, mgr Manager) {}

func migrateStoreV103(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV106(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV108(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV109(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV110(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV111(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV113(ctx cosmos.Context, mgr *Mgrs) {}

func migrateStoreV114(ctx cosmos.Context, mgr *Mgrs) {}

// migrateStoreV116 subset of mainnet migration
func migrateStoreV116(ctx cosmos.Context, mgr *Mgrs) {
	bondKarmaOver := cosmos.NewUint(6936522592883)
	asgardKarmaUnder := cosmos.NewUint(5082320319988)
	ddchainKarmaOver := cosmos.NewUint(100000000)

	actions := []ModuleBalanceAction{
		// send karma from bond oversolvency to fix asgard insolvency
		{
			ModuleName:     BondName,
			KarmaRecipient:  AsgardName,
			KarmaToTransfer: asgardKarmaUnder,
			SynthsToBurn:   common.Coins{},
		},

		// send remaining bond karma oversolvency to reserve
		{
			ModuleName:     BondName,
			KarmaRecipient:  ReserveName,
			KarmaToTransfer: common.SafeSub(bondKarmaOver, asgardKarmaUnder),
			SynthsToBurn:   common.Coins{},
		},

		// transfer karma from ddchain to reserve to clear ddchain balances
		{
			ModuleName:     ModuleName,
			KarmaRecipient:  ReserveName,
			KarmaToTransfer: ddchainKarmaOver,
			SynthsToBurn:   common.Coins{},
		},

		// burn synths from asgard to fix oversolvencies
		{
			ModuleName:     AsgardName,
			KarmaRecipient:  AsgardName, // noop
			KarmaToTransfer: cosmos.ZeroUint(),
			SynthsToBurn: common.Coins{
				{
					Asset:  common.AVAXAsset.GetSyntheticAsset(),
					Amount: cosmos.NewUint(1000001),
				},
			},
		},
	}

	processModuleBalanceActions(ctx, mgr.Keeper(), actions)
}

func migrateStoreV117(ctx cosmos.Context, mgr *Mgrs) {}
