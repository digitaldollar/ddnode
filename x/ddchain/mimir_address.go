//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package ddchain

// ADMINS hard coded admin address
var ADMINS = []string{
	"dd1f29rypfzupm926vyy7v8pu4r508lkg0gj652hh",
	"dd1spycspr76w6da7nd26f4qjngmsdr7eh5x4fvc9",
}
