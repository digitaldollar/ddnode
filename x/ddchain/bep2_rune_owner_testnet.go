//go:build testnet || mocknet
// +build testnet mocknet

package ddchain

// BEP2KarmaOwnerAddress is the owner of BEP2 testnet KARMA address,  during migration all upgraded BEP2 KARMA will be send to this owner address
// DDChain admin will burn those upgraded KARMA appropriately , It need to send to owner address is because only owner can burn it
const BEP2KarmaOwnerAddress = "bnb1lg9yns9zxay9jsf4gvdksn2vdps20q9pqzea03"
