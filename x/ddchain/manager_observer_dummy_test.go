package ddchain

import (
	"gitlab.com/digitaldollar/ddnode/common"
	cosmos "gitlab.com/digitaldollar/ddnode/common/cosmos"
	keeper "gitlab.com/digitaldollar/ddnode/x/ddchain/keeper"
)

type DummyObserverManager struct{}

func NewDummyObserverManager() *DummyObserverManager {
	return &DummyObserverManager{}
}

func (m *DummyObserverManager) BeginBlock()                                                  {}
func (m *DummyObserverManager) EndBlock(ctx cosmos.Context, keeper keeper.Keeper)            {}
func (m *DummyObserverManager) AppendObserver(chain common.Chain, addrs []cosmos.AccAddress) {}
func (m *DummyObserverManager) List() []cosmos.AccAddress                                    { return nil }
