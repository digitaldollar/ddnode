package ddchain

import (
	"bytes"
	"errors"
	"fmt"
	"regexp"

	"github.com/blang/semver"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
	"gitlab.com/digitaldollar/ddnode/constants"
	"gitlab.com/digitaldollar/ddnode/x/ddchain/types"
)

var IsValidDDNameV1 = regexp.MustCompile(`^[a-zA-Z0-9+_-]+$`).MatchString

// ManageDDNameHandler a handler to process MsgNetworkFee messages
type ManageDDNameHandler struct {
	mgr Manager
}

// NewManageDDNameHandler create a new instance of network fee handler
func NewManageDDNameHandler(mgr Manager) ManageDDNameHandler {
	return ManageDDNameHandler{mgr: mgr}
}

// Run is the main entry point for network fee logic
func (h ManageDDNameHandler) Run(ctx cosmos.Context, m cosmos.Msg) (*cosmos.Result, error) {
	msg, ok := m.(*MsgManageDDName)
	if !ok {
		return nil, errInvalidMessage
	}
	if err := h.validate(ctx, *msg); err != nil {
		ctx.Logger().Error("MsgManageDDName failed validation", "error", err)
		return nil, err
	}
	result, err := h.handle(ctx, *msg)
	if err != nil {
		ctx.Logger().Error("fail to process MsgManageDDName", "error", err)
	}
	return result, err
}

func (h ManageDDNameHandler) validate(ctx cosmos.Context, msg MsgManageDDName) error {
	version := h.mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.116.0")):
		return h.validateV116(ctx, msg)
	case version.GTE(semver.MustParse("1.112.0")):
		return h.validateV112(ctx, msg)
	case version.GTE(semver.MustParse("1.110.0")):
		return h.validateV110(ctx, msg)
	case version.GTE(semver.MustParse("0.1.0")):
		return h.validateV1(ctx, msg)
	default:
		return errBadVersion
	}
}

func (h ManageDDNameHandler) validateNameV1(n string) error {
	// validate DDName
	if len(n) > 30 {
		return errors.New("DDName cannot exceed 30 characters")
	}
	if !IsValidDDNameV1(n) {
		return errors.New("invalid DDName")
	}
	return nil
}

func (h ManageDDNameHandler) validateV116(ctx cosmos.Context, msg MsgManageDDName) error {
	if err := msg.ValidateBasic(); err != nil {
		return err
	}

	// TODO on hard fork move network check to ValidateBasic
	if !common.CurrentChainNetwork.SoftEquals(msg.Address.GetNetwork(h.mgr.GetVersion(), msg.Address.GetChain())) {
		return fmt.Errorf("address(%s) is not same network", msg.Address)
	}

	exists := h.mgr.Keeper().DDNameExists(ctx, msg.Name)

	if !exists {
		// ddname doesn't appear to exist, let's validate the name
		if err := h.validateNameV1(msg.Name); err != nil {
			return err
		}
		registrationFee := h.mgr.Keeper().GetDDNameRegisterFee(ctx)
		if msg.Coin.Amount.LTE(registrationFee) {
			return fmt.Errorf("not enough funds")
		}
	} else {
		name, err := h.mgr.Keeper().GetDDName(ctx, msg.Name)
		if err != nil {
			return err
		}

		// if this ddname is already owned, check signer has ownership. If
		// expiration is past, allow different user to take ownership
		if !name.Owner.Equals(msg.Signer) && ctx.BlockHeight() <= name.ExpireBlockHeight {
			ctx.Logger().Error("no authorization", "owner", name.Owner)
			return fmt.Errorf("no authorization: owned by %s", name.Owner)
		}

		// ensure user isn't inflating their expire block height artificaially
		if name.ExpireBlockHeight < msg.ExpireBlockHeight {
			return errors.New("cannot artificially inflate expire block height")
		}
	}

	// validate preferred asset pool exists and is active
	if !msg.PreferredAsset.IsEmpty() {
		if !h.mgr.Keeper().PoolExist(ctx, msg.PreferredAsset) {
			return fmt.Errorf("pool %s does not exist", msg.PreferredAsset)
		}
		pool, err := h.mgr.Keeper().GetPool(ctx, msg.PreferredAsset)
		if err != nil {
			return err
		}
		if pool.Status != PoolAvailable {
			return fmt.Errorf("pool %s is not available", msg.PreferredAsset)
		}
	}

	return nil
}

// handle process MsgManageDDName
func (h ManageDDNameHandler) handle(ctx cosmos.Context, msg MsgManageDDName) (*cosmos.Result, error) {
	version := h.mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.116.0")):
		return h.handleV116(ctx, msg)
	case version.GTE(semver.MustParse("1.112.0")):
		return h.handleV112(ctx, msg)
	case version.GTE(semver.MustParse("0.1.0")):
		return h.handleV1(ctx, msg)
	}
	return nil, errBadVersion
}

// handle process MsgManageDDName
func (h ManageDDNameHandler) handleV116(ctx cosmos.Context, msg MsgManageDDName) (*cosmos.Result, error) {
	var err error

	enable, _ := h.mgr.Keeper().GetMimir(ctx, "DDNames")
	if enable == 0 {
		return nil, fmt.Errorf("DDNames are currently disabled")
	}

	tn := DDName{Name: msg.Name, Owner: msg.Signer, PreferredAsset: common.EmptyAsset}
	exists := h.mgr.Keeper().DDNameExists(ctx, msg.Name)
	if exists {
		tn, err = h.mgr.Keeper().GetDDName(ctx, msg.Name)
		if err != nil {
			return nil, err
		}
	}

	registrationFeePaid := cosmos.ZeroUint()
	fundPaid := cosmos.ZeroUint()

	// check if user is trying to extend expiration
	if !msg.Coin.Amount.IsZero() {
		// check that DDName is still valid, can't top up an invalid DDName
		if err := h.validateNameV1(msg.Name); err != nil {
			return nil, err
		}
		var addBlocks int64
		// registration fee is for DDChain addresses only
		if !exists {
			// minus registration fee
			registrationFee := h.mgr.Keeper().GetDDNameRegisterFee(ctx)
			msg.Coin.Amount = common.SafeSub(msg.Coin.Amount, registrationFee)
			registrationFeePaid = registrationFee
			addBlocks = h.mgr.GetConstants().GetInt64Value(constants.BlocksPerYear) // registration comes with 1 free year
		}
		feePerBlock := h.mgr.Keeper().GetDDNamePerBlockFee(ctx)
		fundPaid = msg.Coin.Amount
		addBlocks += (int64(msg.Coin.Amount.Uint64()) / int64(feePerBlock.Uint64()))
		if tn.ExpireBlockHeight < ctx.BlockHeight() {
			tn.ExpireBlockHeight = ctx.BlockHeight() + addBlocks
		} else {
			tn.ExpireBlockHeight += addBlocks
		}
	}

	// check if we need to reduce the expire time, upon user request
	if msg.ExpireBlockHeight > 0 && msg.ExpireBlockHeight < tn.ExpireBlockHeight {
		tn.ExpireBlockHeight = msg.ExpireBlockHeight
	}

	// check if we need to update the preferred asset
	if !tn.PreferredAsset.Equals(msg.PreferredAsset) && !msg.PreferredAsset.IsEmpty() {
		tn.PreferredAsset = msg.PreferredAsset
	}

	tn.SetAlias(msg.Chain, msg.Address) // update address
	// Update owner if it has changed
	// Also, if owner has changed, null out the PreferredAsset/Aliases so the new owner is forced to reset it.
	if !msg.Owner.Empty() && !bytes.Equal(msg.Owner, tn.Owner) {
		tn.Owner = msg.Owner
		tn.PreferredAsset = common.EmptyAsset
		tn.Aliases = []types.DDNameAlias{}
	}
	h.mgr.Keeper().SetDDName(ctx, tn)

	evt := NewEventDDName(tn.Name, msg.Chain, msg.Address, registrationFeePaid, fundPaid, tn.ExpireBlockHeight, tn.Owner)
	if err := h.mgr.EventMgr().EmitEvent(ctx, evt); nil != err {
		ctx.Logger().Error("fail to emit DDName event", "error", err)
	}

	return &cosmos.Result{}, nil
}
