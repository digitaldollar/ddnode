//go:build !testnet && !mocknet
// +build !testnet,!mocknet

package ddchain

// BEP2KarmaOwnerAddress this is the BEP2 KARMA owner address , during migration all upgraded BEP2 KARMA will be send to this owner address
// DDChain admin will burn those upgraded KARMA appropriately , It need to send to owner address is because only owner can burn it
const BEP2KarmaOwnerAddress = "bnb1e4q8whcufp6d72w8nwmpuhxd96r4n0fstegyuy"
