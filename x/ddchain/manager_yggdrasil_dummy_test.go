package ddchain

import (
	cosmos "gitlab.com/digitaldollar/ddnode/common/cosmos"
)

type DummyYggManager struct{}

func NewDummyYggManger() *DummyYggManager {
	return &DummyYggManager{}
}

func (DummyYggManager) Fund(ctx cosmos.Context, mgr Manager) error {
	return errKaboom
}
