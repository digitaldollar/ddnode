package ddchain

import (
	. "gopkg.in/check.v1"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
	"gitlab.com/digitaldollar/ddnode/constants"
	"gitlab.com/digitaldollar/ddnode/x/ddchain/keeper"
)

type HandlerManageDDNameSuite struct{}

var _ = Suite(&HandlerManageDDNameSuite{})

type KeeperManageDDNameTest struct {
	keeper.Keeper
}

func NewKeeperManageDDNameTest(k keeper.Keeper) KeeperManageDDNameTest {
	return KeeperManageDDNameTest{Keeper: k}
}

func (s *HandlerManageDDNameSuite) TestValidator(c *C) {
	ctx, mgr := setupManagerForTest(c)

	handler := NewManageDDNameHandler(mgr)
	coin := common.NewCoin(common.KarmaAsset(), cosmos.NewUint(100*common.One))
	addr := GetRandomDDAddress()
	acc, _ := addr.AccAddress()
	name := NewDDName("hello", 50, []DDNameAlias{{Chain: common.DDChain, Address: addr}})
	mgr.Keeper().SetDDName(ctx, name)

	// set pool for preferred asset
	pool, err := mgr.Keeper().GetPool(ctx, common.BNBAsset)
	c.Assert(err, IsNil)
	pool.Asset = common.BNBAsset
	err = mgr.Keeper().SetPool(ctx, pool)
	c.Assert(err, IsNil)

	// happy path
	msg := NewMsgManageDDName("I-am_the_99th_walrus+", common.DDChain, addr, coin, 0, common.BNBAsset, acc, acc)
	c.Assert(handler.validate(ctx, *msg), IsNil)

	// fail: address is wrong chain
	msg.Chain = common.BNBChain
	c.Assert(handler.validate(ctx, *msg), NotNil)

	// fail: address is wrong network
	mainnetBNBAddr, err := common.NewAddress("bnb1j08ys4ct2hzzc2hcz6h2hgrvlmsjynawtf2n0y")
	c.Assert(err, IsNil)
	msg.Address = mainnetBNBAddr
	c.Assert(handler.validate(ctx, *msg), NotNil)

	// restore to happy path
	msg.Chain = common.DDChain
	msg.Address = addr

	// fail: name is too long
	msg.Name = "this_name_is_way_too_long_to_be_a_valid_name"
	c.Assert(handler.validate(ctx, *msg), NotNil)

	// fail: bad characters
	msg.Name = "i am the walrus"
	c.Assert(handler.validate(ctx, *msg), NotNil)

	// fail: bad attempt to inflate expire block height
	msg.Name = "hello"
	msg.ExpireBlockHeight = 100
	c.Assert(handler.validate(ctx, *msg), NotNil)

	// fail: bad auth
	msg.ExpireBlockHeight = 0
	msg.Signer = GetRandomBech32Addr()
	c.Assert(handler.validate(ctx, *msg), NotNil)

	// fail: not enough funds for new DDName
	msg.Name = "bang"
	msg.Coin.Amount = cosmos.ZeroUint()
	c.Assert(handler.validate(ctx, *msg), NotNil)
}

func (s *HandlerManageDDNameSuite) TestHandler(c *C) {
	ver := GetCurrentVersion()
	constAccessor := constants.GetConstantValues(ver)
	feePerBlock := constAccessor.GetInt64Value(constants.TNSFeePerBlock)
	registrationFee := constAccessor.GetInt64Value(constants.TNSRegisterFee)
	ctx, mgr := setupManagerForTest(c)

	blocksPerYear := mgr.GetConstants().GetInt64Value(constants.BlocksPerYear)
	handler := NewManageDDNameHandler(mgr)
	coin := common.NewCoin(common.KarmaAsset(), cosmos.NewUint(100*common.One))
	addr := GetRandomDDAddress()
	acc, _ := addr.AccAddress()
	tnName := "hello"

	// add karma to addr for gas
	FundAccount(c, ctx, mgr.Keeper(), acc, 10*common.One)

	// happy path, register new name
	msg := NewMsgManageDDName(tnName, common.DDChain, addr, coin, 0, common.KarmaAsset(), acc, acc)
	_, err := handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	name, err := mgr.Keeper().GetDDName(ctx, tnName)
	c.Assert(err, IsNil)
	c.Check(name.Owner.Empty(), Equals, false)
	c.Check(name.ExpireBlockHeight, Equals, ctx.BlockHeight()+blocksPerYear+(int64(coin.Amount.Uint64())-registrationFee)/feePerBlock)

	// happy path, set alt chain address
	bnbAddr := GetRandomBNBAddress()
	msg = NewMsgManageDDName(tnName, common.BNBChain, bnbAddr, coin, 0, common.KarmaAsset(), acc, acc)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	name, err = mgr.Keeper().GetDDName(ctx, tnName)
	c.Assert(err, IsNil)
	c.Check(name.GetAlias(common.BNBChain).Equals(bnbAddr), Equals, true)

	// happy path, update alt chain address
	bnbAddr = GetRandomBNBAddress()
	msg = NewMsgManageDDName(tnName, common.BNBChain, bnbAddr, coin, 0, common.KarmaAsset(), acc, acc)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	name, err = mgr.Keeper().GetDDName(ctx, tnName)
	c.Assert(err, IsNil)
	c.Check(name.GetAlias(common.BNBChain).Equals(bnbAddr), Equals, true)

	// update preferred asset
	msg = NewMsgManageDDName(tnName, common.BNBChain, bnbAddr, coin, 0, common.BNBAsset, acc, acc)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	name, err = mgr.Keeper().GetDDName(ctx, tnName)
	c.Assert(err, IsNil)
	c.Check(name.GetPreferredAsset(), Equals, common.BNBAsset)

	// transfer ddname to new owner, should reset preferred asset/external aliases
	addr2 := GetRandomDDAddress()
	acc2, _ := addr2.AccAddress()
	msg = NewMsgManageDDName(tnName, common.DDChain, addr, coin, 0, common.KarmaAsset(), acc2, acc)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	name, err = mgr.Keeper().GetDDName(ctx, tnName)
	c.Assert(err, IsNil)
	c.Check(len(name.GetAliases()), Equals, 0)
	c.Check(name.GetPreferredAsset().IsEmpty(), Equals, true)
	c.Check(name.GetOwner().Equals(acc2), Equals, true)

	// happy path, release ddname back into the wild
	msg = NewMsgManageDDName(tnName, common.DDChain, addr, common.NewCoin(common.KarmaAsset(), cosmos.ZeroUint()), 1, common.KarmaAsset(), acc, acc)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	name, err = mgr.Keeper().GetDDName(ctx, tnName)
	c.Assert(err, IsNil)
	c.Check(name.Owner.Empty(), Equals, true)
	c.Check(name.ExpireBlockHeight, Equals, int64(0))
}
