package types

import (
	"gitlab.com/digitaldollar/ddnode/common"
	cosmos "gitlab.com/digitaldollar/ddnode/common/cosmos"
)

// NewNetwork create a new instance Network it is empty though
func NewNetwork() Network {
	return Network{
		BondRewardKarma:  cosmos.ZeroUint(),
		TotalBondUnits:  cosmos.ZeroUint(),
		BurnedBep2Karma:  cosmos.ZeroUint(), // TODO remove on hard fork
		BurnedErc20Karma: cosmos.ZeroUint(), // TODO remove on hard fork
	}
}

// CalcNodeRewards calculate node rewards
func (m *Network) CalcNodeRewards(nodeUnits cosmos.Uint) cosmos.Uint {
	return common.GetUncappedShare(nodeUnits, m.TotalBondUnits, m.BondRewardKarma)
}
