package types

import (
	"gitlab.com/digitaldollar/ddnode/common"
	cosmos "gitlab.com/digitaldollar/ddnode/common/cosmos"
)

// NewMsgManageDDName create a new instance of MsgManageDDName
func NewMsgManageDDName(name string, chain common.Chain, addr common.Address, coin common.Coin, exp int64, asset common.Asset, owner, signer cosmos.AccAddress) *MsgManageDDName {
	return &MsgManageDDName{
		Name:              name,
		Chain:             chain,
		Address:           addr,
		Coin:              coin,
		ExpireBlockHeight: exp,
		PreferredAsset:    asset,
		Owner:             owner,
		Signer:            signer,
	}
}

// Route should return the Route of the module
func (m *MsgManageDDName) Route() string { return RouterKey }

// Type should return the action
func (m MsgManageDDName) Type() string { return "manage_ddname" }

// ValidateBasic runs stateless checks on the message
func (m *MsgManageDDName) ValidateBasic() error {
	// validate n
	if m.Signer.Empty() {
		return cosmos.ErrInvalidAddress(m.Signer.String())
	}
	if m.Chain.IsEmpty() {
		return cosmos.ErrUnknownRequest("chain can't be empty")
	}
	if m.Address.IsEmpty() {
		return cosmos.ErrUnknownRequest("address can't be empty")
	}
	if !m.Address.IsChain(m.Chain) {
		return cosmos.ErrUnknownRequest("address and chain must match")
	}
	if !m.Coin.Asset.IsNativeKarma() {
		return cosmos.ErrUnknownRequest("coin must be native karma")
	}
	return nil
}

// GetSignBytes encodes the message for signing
func (m *MsgManageDDName) GetSignBytes() []byte {
	return cosmos.MustSortJSON(ModuleCdc.MustMarshalJSON(m))
}

// GetSigners defines whose signature is required
func (m *MsgManageDDName) GetSigners() []cosmos.AccAddress {
	return []cosmos.AccAddress{m.Signer}
}
