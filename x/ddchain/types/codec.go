package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"

	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

func init() {
	RegisterCodec(amino)
}

// RegisterCodec register the msg types for amino
func RegisterCodec(cdc *codec.LegacyAmino) {
	cdc.RegisterConcrete(&MsgSwap{}, "ddchain/Swap", nil)
	cdc.RegisterConcrete(&MsgTssPool{}, "ddchain/TssPool", nil)
	cdc.RegisterConcrete(&MsgTssKeysignFail{}, "ddchain/TssKeysignFail", nil)
	cdc.RegisterConcrete(&MsgAddLiquidity{}, "ddchain/AddLiquidity", nil)
	cdc.RegisterConcrete(&MsgWithdrawLiquidity{}, "ddchain/WidthdrawLiquidity", nil)
	cdc.RegisterConcrete(&MsgObservedTxIn{}, "ddchain/ObservedTxIn", nil)
	cdc.RegisterConcrete(&MsgObservedTxOut{}, "ddchain/ObservedTxOut", nil)
	cdc.RegisterConcrete(&MsgDonate{}, "ddchain/MsgDonate", nil)
	cdc.RegisterConcrete(&MsgBond{}, "ddchain/MsgBond", nil)
	cdc.RegisterConcrete(&MsgUnBond{}, "ddchain/MsgUnBond", nil)
	cdc.RegisterConcrete(&MsgLeave{}, "ddchain/MsgLeave", nil)
	cdc.RegisterConcrete(&MsgNoOp{}, "ddchain/MsgNoOp", nil)
	cdc.RegisterConcrete(&MsgOutboundTx{}, "ddchain/MsgOutboundTx", nil)
	cdc.RegisterConcrete(&MsgSetVersion{}, "ddchain/MsgSetVersion", nil)
	cdc.RegisterConcrete(&MsgSetNodeKeys{}, "ddchain/MsgSetNodeKeys", nil)
	cdc.RegisterConcrete(&MsgSetIPAddress{}, "ddchain/MsgSetIPAddress", nil)
	cdc.RegisterConcrete(&MsgYggdrasil{}, "ddchain/MsgYggdrasil", nil)
	cdc.RegisterConcrete(&MsgReserveContributor{}, "ddchain/MsgReserveContributor", nil)
	cdc.RegisterConcrete(&MsgErrataTx{}, "ddchain/MsgErrataTx", nil)
	cdc.RegisterConcrete(&MsgBan{}, "ddchain/MsgBan", nil)
	cdc.RegisterConcrete(&MsgSwitch{}, "ddchain/MsgSwitch", nil) // TODO remove on hard fork
	cdc.RegisterConcrete(&MsgMimir{}, "ddchain/MsgMimir", nil)
	cdc.RegisterConcrete(&MsgDeposit{}, "ddchain/MsgDeposit", nil)
	cdc.RegisterConcrete(&MsgNetworkFee{}, "ddchain/MsgNetworkFee", nil)
	cdc.RegisterConcrete(&MsgMigrate{}, "ddchain/MsgMigrate", nil)
	cdc.RegisterConcrete(&MsgRagnarok{}, "ddchain/MsgRagnarok", nil)
	cdc.RegisterConcrete(&MsgRefundTx{}, "ddchain/MsgRefundTx", nil)
	cdc.RegisterConcrete(&MsgSend{}, "ddchain/MsgSend", nil)
	cdc.RegisterConcrete(&MsgNodePauseChain{}, "ddchain/MsgNodePauseChain", nil)
	cdc.RegisterConcrete(&MsgSolvency{}, "ddchain/MsgSolvency", nil)
	cdc.RegisterConcrete(&MsgManageDDName{}, "ddchain/MsgManageDDName", nil)
}

// RegisterInterfaces register the types
func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSwap{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgTssPool{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgTssKeysignFail{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgAddLiquidity{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgWithdrawLiquidity{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgObservedTxIn{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgObservedTxOut{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgDonate{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgBond{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgUnBond{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgLeave{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNoOp{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgOutboundTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetVersion{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetNodeKeys{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSetIPAddress{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgYggdrasil{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgReserveContributor{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgErrataTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgBan{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSwitch{}) // TODO remove on hard fork
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgMimir{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgDeposit{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNetworkFee{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgMigrate{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgRagnarok{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgRefundTx{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSend{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgNodePauseChain{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgManageDDName{})
	registry.RegisterImplementations((*cosmos.Msg)(nil), &MsgSolvency{})
}
