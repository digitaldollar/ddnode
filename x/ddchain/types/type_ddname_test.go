package types

import (
	. "gopkg.in/check.v1"

	"gitlab.com/digitaldollar/ddnode/common"
)

type DDNameSuite struct{}

var _ = Suite(&DDNameSuite{})

func (DDNameSuite) TestDDName(c *C) {
	// happy path
	n := NewDDName("iamthewalrus", 0, []DDNameAlias{{Chain: common.DDChain, Address: GetRandomDDAddress()}})
	c.Check(n.Valid(), IsNil)

	// unhappy path
	n1 := NewDDName("", 0, []DDNameAlias{{Chain: common.BNBChain, Address: GetRandomDDAddress()}})
	c.Check(n1.Valid(), NotNil)
	n2 := NewDDName("hello", 0, []DDNameAlias{{Chain: common.EmptyChain, Address: GetRandomDDAddress()}})
	c.Check(n2.Valid(), NotNil)
	n3 := NewDDName("hello", 0, []DDNameAlias{{Chain: common.DDChain, Address: common.Address("")}})
	c.Check(n3.Valid(), NotNil)

	// set/get alias
	eth1 := GetRandomETHAddress()
	n1.SetAlias(common.ETHChain, eth1)
	c.Check(n1.GetAlias(common.ETHChain), Equals, eth1)
}
