//go:build testnet
// +build testnet

package ddchain

// ADMINS hard coded admin address
var ADMINS = []string{
	"dd1xghvhe4p50aqh5zq2t2vls938as0dkr2l4e33j",
	"dd19pkncem64gajdwrd5kasspyj0t75hhkpy9zyej",
}
