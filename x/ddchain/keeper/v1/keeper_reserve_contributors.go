package keeperv1

import (
	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

// AddPoolFeeToReserve add fee to reserve, the fee is always in KARMA
func (k KVStore) AddPoolFeeToReserve(ctx cosmos.Context, fee cosmos.Uint) error {
	coin := common.NewCoin(common.KarmaNative, fee)
	sdkErr := k.SendFromModuleToModule(ctx, AsgardName, ReserveName, common.NewCoins(coin))
	if sdkErr != nil {
		return dbError(ctx, "fail to send pool fee to reserve", sdkErr)
	}
	return nil
}

// AddBondFeeToReserve add fee to reserve, the fee is always in KARMA
func (k KVStore) AddBondFeeToReserve(ctx cosmos.Context, fee cosmos.Uint) error {
	coin := common.NewCoin(common.KarmaNative, fee)
	sdkErr := k.SendFromModuleToModule(ctx, BondName, ReserveName, common.NewCoins(coin))
	if sdkErr != nil {
		return dbError(ctx, "fail to send bond fee to reserve", sdkErr)
	}
	return nil
}
