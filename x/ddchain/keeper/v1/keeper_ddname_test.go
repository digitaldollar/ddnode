package keeperv1

import (
	"gitlab.com/digitaldollar/ddnode/common"
	. "gopkg.in/check.v1"
)

type KeeperDDNameSuite struct{}

var _ = Suite(&KeeperDDNameSuite{})

func (s *KeeperDDNameSuite) TestDDName(c *C) {
	ctx, k := setupKeeperForTest(c)
	var err error
	ref := "helloworld"

	ok := k.DDNameExists(ctx, ref)
	c.Assert(ok, Equals, false)

	ddAddr := GetRandomDDAddress()
	bnbAddr := GetRandomBNBAddress()
	name := NewDDName(ref, 50, []DDNameAlias{{Chain: common.DDChain, Address: ddAddr}, {Chain: common.BNBChain, Address: bnbAddr}})
	k.SetDDName(ctx, name)

	ok = k.DDNameExists(ctx, ref)
	c.Assert(ok, Equals, true)
	ok = k.DDNameExists(ctx, "bogus")
	c.Assert(ok, Equals, false)

	name, err = k.GetDDName(ctx, ref)
	c.Assert(err, IsNil)
	c.Assert(name.GetAlias(common.DDChain).Equals(ddAddr), Equals, true)
	c.Assert(name.GetAlias(common.BNBChain).Equals(bnbAddr), Equals, true)
}
