package keeperv1

import (
	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

func (k KVStore) DollarInKarma(ctx cosmos.Context) cosmos.Uint {
	// check for mimir override
	dollarInKarma, err := k.GetMimir(ctx, "DollarInKarma")
	if err == nil && dollarInKarma > 0 {
		return cosmos.NewUint(uint64(dollarInKarma))
	}

	usdAssets := k.GetAnchors(ctx, common.TOR)

	return k.AnchorMedian(ctx, usdAssets)
}
