package keeperv1

import (
	. "gopkg.in/check.v1"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

type KeeperReserveContributorsSuite struct{}

var _ = Suite(&KeeperReserveContributorsSuite{})

func (KeeperReserveContributorsSuite) TestReserveContributors(c *C) {
	ctx, k := setupKeeperForTest(c)

	poolFee := cosmos.NewUint(common.One * 100)
	FundModule(c, ctx, k, AsgardName, poolFee.Uint64())
	asgardBefore := k.GetKarmaBalanceOfModule(ctx, AsgardName)
	reserveBefore := k.GetKarmaBalanceOfModule(ctx, ReserveName)

	c.Assert(k.AddPoolFeeToReserve(ctx, poolFee), IsNil)

	asgardAfter := k.GetKarmaBalanceOfModule(ctx, AsgardName)
	reserveAfter := k.GetKarmaBalanceOfModule(ctx, ReserveName)
	c.Assert(asgardAfter.String(), Equals, asgardBefore.Sub(poolFee).String())
	c.Assert(reserveAfter.String(), Equals, reserveBefore.Add(poolFee).String())

	bondFee := cosmos.NewUint(common.One * 200)
	FundModule(c, ctx, k, BondName, bondFee.Uint64())
	bondBefore := k.GetKarmaBalanceOfModule(ctx, BondName)
	reserveBefore = reserveAfter

	c.Assert(k.AddBondFeeToReserve(ctx, bondFee), IsNil)

	bondAfter := k.GetKarmaBalanceOfModule(ctx, BondName)
	reserveAfter = k.GetKarmaBalanceOfModule(ctx, ReserveName)
	c.Assert(bondAfter.String(), Equals, bondBefore.Sub(bondFee).String())
	c.Assert(reserveAfter.String(), Equals, reserveBefore.Add(bondFee).String())
}
