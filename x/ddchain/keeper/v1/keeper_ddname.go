package keeperv1

import (
	"fmt"

	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

func (k KVStore) setDDName(ctx cosmos.Context, key string, record DDName) {
	store := ctx.KVStore(k.storeKey)
	buf := k.cdc.MustMarshal(&record)
	if buf == nil {
		store.Delete([]byte(key))
	} else {
		store.Set([]byte(key), buf)
	}
}

func (k KVStore) getDDName(ctx cosmos.Context, key string, record *DDName) (bool, error) {
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(key)) {
		return false, nil
	}

	bz := store.Get([]byte(key))
	if err := k.cdc.Unmarshal(bz, record); err != nil {
		return true, dbError(ctx, fmt.Sprintf("Unmarshal kvstore: (%T) %s", record, key), err)
	}

	return true, nil
}

// GetDDNameIterator only iterate DDNames
func (k KVStore) GetDDNameIterator(ctx cosmos.Context) cosmos.Iterator {
	return k.getIterator(ctx, prefixDDName)
}

// SetDDName save the DDName object to store
func (k KVStore) SetDDName(ctx cosmos.Context, name DDName) {
	k.setDDName(ctx, k.GetKey(ctx, prefixDDName, name.Key()), name)
}

// DDNameExists check whether the given name exists
func (k KVStore) DDNameExists(ctx cosmos.Context, name string) bool {
	record := DDName{
		Name: name,
	}
	if k.has(ctx, k.GetKey(ctx, prefixDDName, record.Key())) {
		record, _ = k.GetDDName(ctx, name)
		return record.ExpireBlockHeight >= ctx.BlockHeight()
	}
	return false
}

// GetDDName get DDName with the given pubkey from data store
func (k KVStore) GetDDName(ctx cosmos.Context, name string) (DDName, error) {
	record := DDName{
		Name: name,
	}
	ok, err := k.getDDName(ctx, k.GetKey(ctx, prefixDDName, record.Key()), &record)
	if !ok {
		return record, fmt.Errorf("DDName doesn't exist: %s", name)
	}
	if record.ExpireBlockHeight < ctx.BlockHeight() {
		return DDName{Name: name}, nil
	}
	return record, err
}

// DeleteDDName remove the given DDName from data store
func (k KVStore) DeleteDDName(ctx cosmos.Context, name string) error {
	n := DDName{Name: name}
	k.del(ctx, k.GetKey(ctx, prefixDDName, n.Key()))
	return nil
}

// AffiliateFeeCollector

func (k KVStore) setAffiliateCollector(ctx cosmos.Context, key string, record AffiliateFeeCollector) {
	store := ctx.KVStore(k.storeKey)
	buf := k.cdc.MustMarshal(&record)
	if buf == nil {
		store.Delete([]byte(key))
	} else {
		store.Set([]byte(key), buf)
	}
}

func (k KVStore) getAffilateCollector(ctx cosmos.Context, key string, record *AffiliateFeeCollector) (bool, error) {
	store := ctx.KVStore(k.storeKey)
	if !store.Has([]byte(key)) {
		return false, nil
	}

	bz := store.Get([]byte(key))
	if err := k.cdc.Unmarshal(bz, record); err != nil {
		return true, dbError(ctx, fmt.Sprintf("Unmarshal kvstore: (%T) %s", record, key), err)
	}

	return true, nil
}

func (k KVStore) SetAffiliateCollector(ctx cosmos.Context, collector AffiliateFeeCollector) {
	k.setAffiliateCollector(ctx, k.GetKey(ctx, prefixAffiliateCollector, collector.OwnerAddress.String()), collector)
}

func (k KVStore) GetAffiliateCollector(ctx cosmos.Context, acc cosmos.AccAddress) (AffiliateFeeCollector, error) {
	record := AffiliateFeeCollector{
		OwnerAddress: acc,
		KarmaAmount:   cosmos.ZeroUint(),
	}
	_, err := k.getAffilateCollector(ctx, k.GetKey(ctx, prefixAffiliateCollector, record.OwnerAddress.String()), &record)
	return record, err
}

func (k KVStore) GetAffiliateCollectorIterator(ctx cosmos.Context) cosmos.Iterator {
	return k.getIterator(ctx, prefixAffiliateCollector)
}

func (k KVStore) GetAffiliateCollectors(ctx cosmos.Context) ([]AffiliateFeeCollector, error) {
	var affCols []AffiliateFeeCollector
	iterator := k.GetAffiliateCollectorIterator(ctx)
	defer iterator.Close()
	for ; iterator.Valid(); iterator.Next() {
		var ac AffiliateFeeCollector
		err := k.Cdc().Unmarshal(iterator.Value(), &ac)
		if err != nil {
			return nil, dbError(ctx, "Unmarsahl: ac", err)
		}
		affCols = append(affCols, ac)
	}
	return affCols, nil
}
