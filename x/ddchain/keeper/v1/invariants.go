package keeperv1

import (
	"fmt"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
)

// InvariantRoutes return the keeper's invariant routes
func (k KVStore) InvariantRoutes() []common.InvariantRoute {
	return []common.InvariantRoute{
		common.NewInvariantRoute("asgard", AsgardInvariant(k)),
		common.NewInvariantRoute("bond", BondInvariant(k)),
		common.NewInvariantRoute("ddchain", DDChainInvariant(k)),
		common.NewInvariantRoute("affiliate_collector", AffilliateCollectorInvariant(k)),
		common.NewInvariantRoute("pools", PoolsInvariant(k)),
		common.NewInvariantRoute("streaming_swaps", StreamingSwapsInvariant(k)),
	}
}

// AsgardInvariant the asgard module backs pool karma, savers synths, and native
// coins in queued swaps
func AsgardInvariant(k KVStore) common.Invariant {
	return func(ctx cosmos.Context) (msg []string, broken bool) {
		// sum all karma liquidity on pools, including pending
		var poolCoins common.Coins
		pools, _ := k.GetPools(ctx)
		for _, pool := range pools {
			switch {
			case pool.Asset.IsSyntheticAsset():
				coin := common.NewCoin(
					pool.Asset,
					pool.BalanceAsset,
				)
				poolCoins = poolCoins.Add(coin)
			case !pool.Asset.IsDerivedAsset():
				coin := common.NewCoin(
					common.KarmaAsset(),
					pool.BalanceKarma.Add(pool.PendingInboundKarma),
				)
				poolCoins = poolCoins.Add(coin)
			}
		}

		// sum all karma in pending swaps
		var swapCoins common.Coins
		swapIter := k.GetSwapQueueIterator(ctx)
		defer swapIter.Close()
		for ; swapIter.Valid(); swapIter.Next() {
			var swap MsgSwap
			k.Cdc().MustUnmarshal(swapIter.Value(), &swap)

			if len(swap.Tx.Coins) != 1 {
				broken = true
				msg = append(msg, fmt.Sprintf("wrong number of coins for swap: %d, %s", len(swap.Tx.Coins), swap.Tx.ID))
				continue
			}

			coin := swap.Tx.Coins[0]
			if !coin.IsNative() {
				continue // only verifying native coins in this invariant
			}

			// adjust for streaming swaps
			ss, err := k.GetStreamingSwap(ctx, swap.Tx.ID)
			if err != nil {
				ctx.Logger().Error("error getting streaming swap", "error", err)
				continue // should never happen
			}
			if !ss.In.IsZero() {
				// adjust for stream swap amount, the amount In has been added
				// to the pool but not deducted from the tx or module, so deduct
				// that In amount from the tx coin
				coin.Amount = coin.Amount.Sub(ss.In)
			}
			swapCoins = swapCoins.Add(coin)
		}

		// get asgard module balance
		asgardAddr := k.GetModuleAccAddress(AsgardName)
		asgardCoins := k.GetBalance(ctx, asgardAddr)

		// asgard balance is expected to equal sum of pool and swap coins
		expNative, _ := poolCoins.Adds(swapCoins).Native()

		// note: coins must be sorted for SafeSub
		diffCoins, _ := asgardCoins.SafeSub(expNative.Sort())
		if !diffCoins.IsZero() {
			broken = true
			for _, coin := range diffCoins {
				if coin.IsPositive() {
					msg = append(msg, fmt.Sprintf("oversolvent: %s", coin))
				} else {
					coin.Amount = coin.Amount.Neg()
					msg = append(msg, fmt.Sprintf("insolvent: %s", coin))
				}
			}
		}

		return msg, broken
	}
}

// BondInvariant the bond module backs node bond and pending reward bond
func BondInvariant(k KVStore) common.Invariant {
	return func(ctx cosmos.Context) (msg []string, broken bool) {
		// sum all karma bonded to nodes
		bondedKarma := cosmos.ZeroUint()
		naIter := k.GetNodeAccountIterator(ctx)
		defer naIter.Close()
		for ; naIter.Valid(); naIter.Next() {
			var na NodeAccount
			k.Cdc().MustUnmarshal(naIter.Value(), &na)
			bondedKarma = bondedKarma.Add(na.Bond)
		}

		// get pending bond reward karma
		network, _ := k.GetNetwork(ctx)
		bondRewardKarma := network.BondRewardKarma

		// get karma balance of bond module
		bondModuleKarma := k.GetBalanceOfModule(ctx, BondName, common.KarmaAsset().Native())

		// bond module is expected to equal bonded karma and pending rewards
		expectedKarma := bondedKarma.Add(bondRewardKarma)
		if expectedKarma.GT(bondModuleKarma) {
			broken = true
			diff := expectedKarma.Sub(bondModuleKarma)
			coin, _ := common.NewCoin(common.KarmaAsset(), diff).Native()
			msg = append(msg, fmt.Sprintf("insolvent: %s", coin))

		} else if expectedKarma.LT(bondModuleKarma) {
			broken = true
			diff := bondModuleKarma.Sub(expectedKarma)
			coin, _ := common.NewCoin(common.KarmaAsset(), diff).Native()
			msg = append(msg, fmt.Sprintf("oversolvent: %s", coin))
		}

		return msg, broken
	}
}

// DDChainInvariant the ddchain module should never hold a balance
func DDChainInvariant(k KVStore) common.Invariant {
	return func(ctx cosmos.Context) (msg []string, broken bool) {
		// module balance of theorchain
		tcAddr := k.GetModuleAccAddress(ModuleName)
		tcCoins := k.GetBalance(ctx, tcAddr)

		// ddchain module should never carry a balance
		if !tcCoins.Empty() {
			broken = true
			for _, coin := range tcCoins {
				msg = append(msg, fmt.Sprintf("oversolvent: %s", coin))
			}
		}

		return msg, broken
	}
}

// AffilliateCollectorInvariant the affiliate_collector module backs accrued affiliate
// rewards
func AffilliateCollectorInvariant(k KVStore) common.Invariant {
	return func(ctx cosmos.Context) (msg []string, broken bool) {
		affColModuleKarma := k.GetBalanceOfModule(ctx, AffiliateCollectorName, common.KarmaAsset().Native())
		affCols, err := k.GetAffiliateCollectors(ctx)
		if err != nil {
			if affColModuleKarma.IsZero() {
				return nil, false
			}
			msg = append(msg, err.Error())
			return msg, true
		}

		totalAffKarma := cosmos.ZeroUint()
		for _, ac := range affCols {
			totalAffKarma = totalAffKarma.Add(ac.KarmaAmount)
		}

		if totalAffKarma.GT(affColModuleKarma) {
			broken = true
			diff := totalAffKarma.Sub(affColModuleKarma)
			coin, _ := common.NewCoin(common.KarmaAsset(), diff).Native()
			msg = append(msg, fmt.Sprintf("insolvent: %s", coin))
		} else if totalAffKarma.LT(affColModuleKarma) {
			broken = true
			diff := affColModuleKarma.Sub(totalAffKarma)
			coin, _ := common.NewCoin(common.KarmaAsset(), diff).Native()
			msg = append(msg, fmt.Sprintf("oversolvent: %s", coin))
		}

		return msg, broken
	}
}

// PoolsInvariant pool units and pending karma/asset should match the sum
// of units and pending karma/asset for all lps
func PoolsInvariant(k KVStore) common.Invariant {
	return func(ctx cosmos.Context) (msg []string, broken bool) {
		pools, _ := k.GetPools(ctx)
		for _, pool := range pools {
			if pool.Asset.IsNative() {
				continue // only looking at layer-one pools
			}

			lpUnits := cosmos.ZeroUint()
			lpPendingKarma := cosmos.ZeroUint()
			lpPendingAsset := cosmos.ZeroUint()

			lpIter := k.GetLiquidityProviderIterator(ctx, pool.Asset)
			defer lpIter.Close()
			for ; lpIter.Valid(); lpIter.Next() {
				var lp LiquidityProvider
				k.Cdc().MustUnmarshal(lpIter.Value(), &lp)
				lpUnits = lpUnits.Add(lp.Units)
				lpPendingKarma = lpPendingKarma.Add(lp.PendingKarma)
				lpPendingAsset = lpPendingAsset.Add(lp.PendingAsset)
			}

			check := func(poolValue, lpValue cosmos.Uint, valueType string) {
				if poolValue.GT(lpValue) {
					diff := poolValue.Sub(lpValue)
					msg = append(msg, fmt.Sprintf("%s oversolvent: %s %s", pool.Asset, diff.String(), valueType))
					broken = true
				} else if poolValue.LT(lpValue) {
					diff := lpValue.Sub(poolValue)
					msg = append(msg, fmt.Sprintf("%s insolvent: %s %s", pool.Asset, diff.String(), valueType))
					broken = true
				}
			}

			check(pool.LPUnits, lpUnits, "units")
			check(pool.PendingInboundKarma, lpPendingKarma, "pending karma")
			check(pool.PendingInboundAsset, lpPendingAsset, "pending asset")
		}

		return msg, broken
	}
}

// StreamingSwapsInvariant every streaming swap should have a corresponding
// queued swap, stream deposit should equal the queued swap's source coin,
// and the stream should be internally consistent
func StreamingSwapsInvariant(k KVStore) common.Invariant {
	return func(ctx cosmos.Context) (msg []string, broken bool) {
		// fetch all streaming swaps from the swap queue
		var swaps []MsgSwap
		swapIter := k.GetSwapQueueIterator(ctx)
		defer swapIter.Close()
		for ; swapIter.Valid(); swapIter.Next() {
			var swap MsgSwap
			k.Cdc().MustUnmarshal(swapIter.Value(), &swap)
			if swap.IsStreaming() {
				swaps = append(swaps, swap)
			}
		}

		// fetch all stream swap records
		var streams []StreamingSwap
		ssIter := k.GetStreamingSwapIterator(ctx)
		defer ssIter.Close()
		for ; ssIter.Valid(); ssIter.Next() {
			var stream StreamingSwap
			k.Cdc().MustUnmarshal(ssIter.Value(), &stream)
			streams = append(streams, stream)
		}

		// should be a 1:1 correlation
		if len(swaps) != len(streams) {
			broken = true
			msg = append(msg, fmt.Sprintf(
				"swap count %d not equal to stream count %d",
				len(swaps),
				len(streams)))
		}

		for _, swap := range swaps {
			found := false
			for _, stream := range streams {
				if !swap.Tx.ID.Equals(stream.TxID) {
					continue
				}
				found = true
				if !swap.Tx.Coins[0].Amount.Equal(stream.Deposit) {
					broken = true
					msg = append(msg, fmt.Sprintf(
						"%s: swap.coin %s != stream.deposit %s",
						stream.TxID.String(),
						swap.Tx.Coins[0].Amount,
						stream.Deposit.String()))
				}
				if stream.Count > stream.Quantity {
					broken = true
					msg = append(msg, fmt.Sprintf(
						"%s: stream.count %d > stream.quantity %d",
						stream.TxID.String(),
						stream.Count,
						stream.Quantity))
				}
				if stream.In.GT(stream.Deposit) {
					broken = true
					msg = append(msg, fmt.Sprintf(
						"%s: stream.in %s > stream.deposit %s",
						stream.TxID.String(),
						stream.In.String(),
						stream.Deposit.String()))
				}
			}
			if !found {
				broken = true
				msg = append(msg, fmt.Sprintf("stream not found for swap: %s", swap.Tx.ID.String()))
			}
		}

		return msg, broken
	}
}
