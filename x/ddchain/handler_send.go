package ddchain

import (
	"errors"
	"fmt"

	"github.com/blang/semver"

	"gitlab.com/digitaldollar/ddnode/common"
	"gitlab.com/digitaldollar/ddnode/common/cosmos"
	"gitlab.com/digitaldollar/ddnode/x/ddchain/keeper"
)

// NewSendHandler create a new instance of SendHandler
func NewSendHandler(mgr Manager) BaseHandler[*MsgSend] {
	return BaseHandler[*MsgSend]{
		mgr:    mgr,
		logger: MsgSendLogger,
		validators: NewValidators[*MsgSend]().
			Register("1.87.0", MsgSendValidateV87).
			Register("0.1.0", MsgSendValidateV1),
		handlers: NewHandlers[*MsgSend]().
			Register("1.116.0", MsgSendHandleV116).
			Register("1.115.0", MsgSendHandleV115).
			Register("1.112.0", MsgSendHandleV112).
			Register("1.108.0", MsgSendHandleV108).
			Register("0.1.0", MsgSendHandleV1),
	}
}

func MsgSendValidateV87(ctx cosmos.Context, mgr Manager, msg *MsgSend) error {
	if err := msg.ValidateBasic(); err != nil {
		return err
	}

	// disallow sends to modules, they should only be interacted with via deposit messages
	if msg.ToAddress.Equals(mgr.Keeper().GetModuleAccAddress(AsgardName)) ||
		msg.ToAddress.Equals(mgr.Keeper().GetModuleAccAddress(BondName)) ||
		msg.ToAddress.Equals(mgr.Keeper().GetModuleAccAddress(ReserveName)) ||
		msg.ToAddress.Equals(mgr.Keeper().GetModuleAccAddress(ModuleName)) {
		return errors.New("cannot use MsgSend for Module transactions, use MsgDeposit instead")
	}

	return nil
}

func MsgSendLogger(ctx cosmos.Context, msg *MsgSend) {
	ctx.Logger().Info("receive MsgSend", "from", msg.FromAddress, "to", msg.ToAddress, "coins", msg.Amount)
}

func MsgSendHandleV116(ctx cosmos.Context, mgr Manager, msg *MsgSend) (*cosmos.Result, error) {
	if mgr.Keeper().IsChainHalted(ctx, common.DDChain) {
		return nil, fmt.Errorf("unable to use MsgSend while DDChain is halted")
	}

	err := mgr.Keeper().SendCoins(ctx, msg.FromAddress, msg.ToAddress, msg.Amount)
	if err != nil {
		return nil, err
	}

	return &cosmos.Result{}, nil
}

// SendAnteHandler called by the ante handler to gate mempool entry
// and also during deliver. Store changes will persist if this function
// succeeds, regardless of the success of the transaction.
func SendAnteHandler(ctx cosmos.Context, v semver.Version, k keeper.Keeper, msg MsgSend) error {
	// TODO remove on hard fork
	if v.LT(semver.MustParse("1.115.0")) {
		nativeTxFee := k.GetNativeTxFee(ctx)
		gas := common.NewCoin(common.KarmaNative, nativeTxFee)
		gasFee, err := gas.Native()
		if err != nil {
			return fmt.Errorf("fail to get gas fee: %w", err)
		}
		totalCoins := cosmos.NewCoins(gasFee)
		if !k.HasCoins(ctx, msg.GetSigners()[0], totalCoins) {
			return cosmos.ErrInsufficientCoins(err, "insufficient funds")
		}
		return nil
	}

	return k.DeductNativeTxFeeFromAccount(ctx, msg.GetSigners()[0])
}
