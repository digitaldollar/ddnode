//go:build stagenet
// +build stagenet

package ddchain

// ADMINS hard coded admin address
var ADMINS = []string{
	"dd1lr9hcvvkzclmcdke5kqx4wxeymeng36p7keq0f",
	"dd1qwm26kmhctj5ezzrasz3cdccpt8kjjcj5fs0pn",
	"dd1ts0txfh3ev7lm9qf839esnartdw62w6lga39t0",
	"dd19pkncem64gajdwrd5kasspyj0t75hhkp5t0zkf",
}
