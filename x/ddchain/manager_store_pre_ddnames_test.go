package ddchain

import (
	"os"

	"gitlab.com/digitaldollar/ddnode/common/cosmos"
	. "gopkg.in/check.v1"
)

type PreDDNameTestSuite struct{}

var _ = Suite(&PreDDNameTestSuite{})

func (s *PreDDNameTestSuite) TestLoadingJson(c *C) {
	// use the mainnet preregister ddnames for test
	var err error
	preregisterDDNames, err = os.ReadFile("preregister_ddnames.json")
	c.Assert(err, IsNil)

	ctx, _ := setupKeeperForTest(c)
	config := cosmos.GetConfig()
	config.SetBech32PrefixForAccount("dd", "ddpub")
	names, err := getPreRegisterDDNames(ctx, 100)
	c.Assert(err, IsNil)
	c.Check(names, HasLen, 9195, Commentf("%d", len(names)))
}
