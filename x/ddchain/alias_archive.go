package ddchain

import (
	mem "gitlab.com/digitaldollar/ddnode/x/ddchain/memo"
	"gitlab.com/digitaldollar/ddnode/x/ddchain/types"
)

var (
	NewEventSwitch    = types.NewEventSwitch
	NewEventSwitchV87 = types.NewEventSwitchV87
	NewMsgSwitch      = types.NewMsgSwitch
)

type (
	MsgSwitch  = types.MsgSwitch
	SwitchMemo = mem.SwitchMemo
)
