//go:build !testnet && !mocknet && !stagenet
// +build !testnet,!mocknet,!stagenet

package ddchain

import _ "embed"

//go:embed preregister_ddnames.json
var preregisterDDNames []byte
