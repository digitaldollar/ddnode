//go:build mocknet
// +build mocknet

package ddchain

// ADMINS hard coded admin address
var ADMINS = []string{
	"dd1xghvhe4p50aqh5zq2t2vls938as0dkr2l4e33j",
	"dd19pkncem64gajdwrd5kasspyj0t75hhkpy9zyej",

	// "dog" mnemonic key for local testing:
	// dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog fossil
	"dd1zf3gsk7edzwl9syyefvfhle37cjtql35g639t0",
}
