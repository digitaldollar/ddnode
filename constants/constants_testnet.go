//go:build testnet
// +build testnet

// For Public TestNet
package constants

func init() {
	int64Overrides = map[ConstantName]int64{
		PoolCycle:                           1000,
		MinKarmaPoolDepth:                    100_00000000,
		AsgardSize:                          15,
		DesiredValidatorSet:                 30,
		ChurnInterval:                       240,
		MinimumBondInKarma:                   10000_00000000, // 10K karma
		LiquidityLockUpBlocks:               0,
		StagedPoolCost:                      10_00000000,
		PreferredAssetOutboundFeeMultiplier: 100,
	}
	boolOverrides = map[ConstantName]bool{
		StrictBondLiquidityRatio: false,
	}
}
