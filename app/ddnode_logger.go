package app

import (
	tmlog "github.com/tendermint/tendermint/libs/log"
	"gitlab.com/digitaldollar/ddnode/log"
)

var _ tmlog.Logger = (*log.TendermintLogWrapper)(nil)
