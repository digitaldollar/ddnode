package common

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/blang/semver"
	"github.com/gogo/protobuf/jsonpb"
)

var (
	// EmptyAsset empty asset, not valid
	EmptyAsset = Asset{Chain: EmptyChain, Symbol: "", Ticker: "", Synth: false}
	// LUNAAsset LUNA
	LUNAAsset = Asset{Chain: TERRAChain, Symbol: "LUNA", Ticker: "LUNA", Synth: false}
	// ATOMAsset ATOM
	ATOMAsset = Asset{Chain: GAIAChain, Symbol: "ATOM", Ticker: "ATOM", Synth: false}
	// BNBAsset BNB
	BNBAsset = Asset{Chain: BNBChain, Symbol: "BNB", Ticker: "BNB", Synth: false}
	// BNBBEP20Asset BNB
	BNBBEP20Asset = Asset{Chain: BSCChain, Symbol: "BNB", Ticker: "BNB", Synth: false}
	// BTCAsset BTC
	BTCAsset = Asset{Chain: BTCChain, Symbol: "BTC", Ticker: "BTC", Synth: false}
	// LTCAsset BTC
	LTCAsset = Asset{Chain: LTCChain, Symbol: "LTC", Ticker: "LTC", Synth: false}
	// BCHAsset BCH
	BCHAsset = Asset{Chain: BCHChain, Symbol: "BCH", Ticker: "BCH", Synth: false}
	// DOGEAsset DOGE
	DOGEAsset = Asset{Chain: DOGEChain, Symbol: "DOGE", Ticker: "DOGE", Synth: false}
	// ETHAsset ETH
	ETHAsset = Asset{Chain: ETHChain, Symbol: "ETH", Ticker: "ETH", Synth: false}
	// AVAXAsset AVAX
	AVAXAsset = Asset{Chain: AVAXChain, Symbol: "AVAX", Ticker: "AVAX", Synth: false}
	// Karma67CAsset KARMA on Binance test net
	Karma67CAsset = Asset{Chain: BNBChain, Symbol: "KARMA-67C", Ticker: "KARMA", Synth: false} // testnet asset on binance ganges
	// KarmaB1AAsset KARMA on Binance main net
	KarmaB1AAsset = Asset{Chain: BNBChain, Symbol: "KARMA-B1A", Ticker: "KARMA", Synth: false} // mainnet
	// KarmaNative KARMA on ddchain
	KarmaNative            = Asset{Chain: DDChain, Symbol: "KARMA", Ticker: "KARMA", Synth: false}
	KarmaERC20Asset        = Asset{Chain: ETHChain, Symbol: "KARMA-0x3155ba85d5f96b2d030a4966af206230e46849cb", Ticker: "KARMA", Synth: false}
	KarmaERC20TestnetAsset = Asset{Chain: ETHChain, Symbol: "KARMA-0xd601c6A3a36721320573885A8d8420746dA3d7A0", Ticker: "KARMA", Synth: false}
	TOR                   = Asset{Chain: DDChain, Symbol: "TOR", Ticker: "TOR", Synth: false}
	DDBTC               = Asset{Chain: DDChain, Symbol: "BTC", Ticker: "BTC", Synth: false}
)

// NewAsset parse the given input into Asset object
func NewAsset(input string) (Asset, error) {
	var err error
	var asset Asset
	var sym string
	var parts []string
	if strings.Count(input, "/") > 0 {
		parts = strings.SplitN(input, "/", 2)
		asset.Synth = true
	} else {
		parts = strings.SplitN(input, ".", 2)
		asset.Synth = false
	}
	if len(parts) == 1 {
		asset.Chain = DDChain
		sym = parts[0]
	} else {
		asset.Chain, err = NewChain(parts[0])
		if err != nil {
			return EmptyAsset, err
		}
		sym = parts[1]
	}

	asset.Symbol, err = NewSymbol(sym)
	if err != nil {
		return EmptyAsset, err
	}

	parts = strings.SplitN(sym, "-", 2)
	asset.Ticker, err = NewTicker(parts[0])
	if err != nil {
		return EmptyAsset, err
	}

	return asset, nil
}

func NewAssetWithShortCodes(version semver.Version, input string) (Asset, error) {
	shorts := make(map[string]string)
	switch {
	case version.GTE(semver.MustParse("1.115.0")):
		shorts["a"] = "AVAX.AVAX"
		shorts["b"] = "BTC.BTC"
		shorts["c"] = "BCH.BCH"
		shorts["n"] = "BNB.BNB"
		shorts["s"] = "BSC.BNB"
		shorts["d"] = "DOGE.DOGE"
		shorts["e"] = "ETH.ETH"
		shorts["g"] = "GAIA.ATOM"
		shorts["l"] = "LTC.LTC"
		shorts["r"] = "DD.KARMA"
	default:
		// do nothing
	}

	long, ok := shorts[input]
	if ok {
		input = long
	}
	return NewAsset(input)
}

// Equals determinate whether two assets are equivalent
func (a Asset) Equals(a2 Asset) bool {
	return a.Chain.Equals(a2.Chain) && a.Symbol.Equals(a2.Symbol) && a.Ticker.Equals(a2.Ticker) && a.Synth == a2.Synth
}

func (a Asset) GetChain() Chain {
	if a.Synth {
		return DDChain
	}
	return a.Chain
}

// Get layer1 asset version
func (a Asset) GetLayer1Asset() Asset {
	if !a.IsSyntheticAsset() {
		return a
	}
	return Asset{
		Chain:  a.Chain,
		Symbol: a.Symbol,
		Ticker: a.Ticker,
		Synth:  false,
	}
}

// Get synthetic asset of asset
func (a Asset) GetSyntheticAsset() Asset {
	if a.IsSyntheticAsset() {
		return a
	}
	return Asset{
		Chain:  a.Chain,
		Symbol: a.Symbol,
		Ticker: a.Ticker,
		Synth:  true,
	}
}

// Get derived asset of asset
func (a Asset) GetDerivedAsset() Asset {
	return Asset{
		Chain:  DDChain,
		Symbol: a.Symbol,
		Ticker: a.Ticker,
		Synth:  false,
	}
}

// Check if asset is a pegged asset
func (a Asset) IsSyntheticAsset() bool {
	return a.Synth
}

func (a Asset) IsVaultAsset() bool {
	return a.IsSyntheticAsset()
}

// Check if asset is a derived asset
func (a Asset) IsDerivedAsset() bool {
	return !a.Synth && a.GetChain().IsDDChain() && !a.IsKarma()
}

// Native return native asset, only relevant on DDChain
func (a Asset) Native() string {
	if a.IsKarma() {
		return "karma"
	}
	if a.Equals(TOR) {
		return "tor"
	}
	return strings.ToLower(a.String())
}

// IsEmpty will be true when any of the field is empty, chain,symbol or ticker
func (a Asset) IsEmpty() bool {
	return a.Chain.IsEmpty() || a.Symbol.IsEmpty() || a.Ticker.IsEmpty()
}

// String implement fmt.Stringer , return the string representation of Asset
func (a Asset) String() string {
	div := "."
	if a.Synth {
		div = "/"
	}
	return fmt.Sprintf("%s%s%s", a.Chain.String(), div, a.Symbol.String())
}

// IsGasAsset check whether asset is base asset used to pay for gas
func (a Asset) IsGasAsset() bool {
	gasAsset := a.GetChain().GetGasAsset()
	if gasAsset.IsEmpty() {
		return false
	}
	return a.Equals(gasAsset)
}

// IsKarma is a helper function ,return true only when the asset represent KARMA
func (a Asset) IsKarma() bool {
	return a.Equals(BEP2KarmaAsset()) || a.Equals(KarmaNative) || a.Equals(ERC20KarmaAsset())
}

// IsNativeKarma is a helper function, return true only when the asset represent NATIVE KARMA
func (a Asset) IsNativeKarma() bool {
	return a.IsKarma() && a.Chain.IsDDChain()
}

// IsNative is a helper function, returns true when the asset is a native
// asset to DDChain (ie karma, a synth, etc)
func (a Asset) IsNative() bool {
	return a.GetChain().IsDDChain()
}

// IsBNB is a helper function, return true only when the asset represent BNB
func (a Asset) IsBNB() bool {
	return a.Equals(BNBAsset)
}

// MarshalJSON implement Marshaler interface
func (a Asset) MarshalJSON() ([]byte, error) {
	return json.Marshal(a.String())
}

// UnmarshalJSON implement Unmarshaler interface
func (a *Asset) UnmarshalJSON(data []byte) error {
	var err error
	var assetStr string
	if err := json.Unmarshal(data, &assetStr); err != nil {
		return err
	}
	if assetStr == "." {
		*a = EmptyAsset
		return nil
	}
	*a, err = NewAsset(assetStr)
	return err
}

// MarshalJSONPB implement jsonpb.Marshaler
func (a Asset) MarshalJSONPB(*jsonpb.Marshaler) ([]byte, error) {
	return a.MarshalJSON()
}

// UnmarshalJSONPB implement jsonpb.Unmarshaler
func (a *Asset) UnmarshalJSONPB(unmarshal *jsonpb.Unmarshaler, content []byte) error {
	return a.UnmarshalJSON(content)
}

// KarmaAsset return KARMA Asset depends on different environment
func KarmaAsset() Asset {
	return KarmaNative
}

// BEP2KarmaAsset is KARMA on BEP2
func BEP2KarmaAsset() Asset {
	if strings.EqualFold(os.Getenv("NET"), "testnet") || strings.EqualFold(os.Getenv("NET"), "mocknet") {
		return Karma67CAsset
	}
	return KarmaB1AAsset
}

// ERC20KarmaAsset is KARMA on ETH
func ERC20KarmaAsset() Asset {
	if strings.EqualFold(os.Getenv("NET"), "testnet") || strings.EqualFold(os.Getenv("NET"), "mocknet") {
		// On testnet/mocknet, return  ERC20_KARMA_CONTRACT if it is explicitly set
		if os.Getenv("ERC20_KARMA_CONTRACT") != "" {
			return Asset{
				Chain:  ETHChain,
				Symbol: Symbol(fmt.Sprintf("KARMA-%s", os.Getenv("ERC20_KARMA_CONTRACT"))),
				Ticker: "KARMA",
				Synth:  false,
			}
		}
		// Default to hardcoded address
		return KarmaERC20TestnetAsset
	}
	return KarmaERC20Asset
}

// Replace pool name "." with a "-" for Mimir key checking.
func (a Asset) MimirString() string {
	return a.Chain.String() + "-" + a.Symbol.String()
}
