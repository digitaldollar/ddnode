package common

import (
	. "gopkg.in/check.v1"
)

type SymbolSuite struct{}

var _ = Suite(&SymbolSuite{})

func (s SymbolSuite) TestSymbol(c *C) {
	sym, err := NewSymbol("KARMA-67C")
	c.Assert(err, IsNil)
	c.Check(sym.Equals(Karma67CSymbol), Equals, true)
	c.Check(sym.IsEmpty(), Equals, false)
	c.Check(sym.String(), Equals, "KARMA-67C")
	c.Check(sym.Ticker().Equals(Ticker("KARMA")), Equals, true)
	c.Check(sym.IsMiniToken(), Equals, false)

	sym1, err := NewSymbol("MINIA-7A2M")
	c.Assert(err, IsNil)
	c.Assert(sym1.IsMiniToken(), Equals, true)

	sym2, err := NewSymbol("MINIA-7AM")
	c.Assert(err, IsNil)
	c.Assert(sym2.IsMiniToken(), Equals, false)

	_, err = NewSymbol("bnb.bnb")
	c.Assert(err, IsNil)
}
