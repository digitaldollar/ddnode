package common

import (
	"github.com/blang/semver"
	. "gopkg.in/check.v1"
)

type AssetSuite struct{}

var _ = Suite(&AssetSuite{})

func (s AssetSuite) TestAsset(c *C) {
	asset, err := NewAsset("dd.karma")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(KarmaNative), Equals, true)
	c.Check(asset.IsKarma(), Equals, true)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.Synth, Equals, false)
	c.Check(asset.String(), Equals, "DD.KARMA")

	asset, err = NewAsset("dd/karma")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(KarmaNative), Equals, false)
	c.Check(asset.IsKarma(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.Synth, Equals, true)
	c.Check(asset.String(), Equals, "DD/KARMA")

	c.Check(asset.Chain.Equals(DDChain), Equals, true)
	c.Check(asset.Symbol.Equals(Symbol("KARMA")), Equals, true)
	c.Check(asset.Ticker.Equals(Ticker("KARMA")), Equals, true)

	asset, err = NewAsset("BNB.SWIPE.B-DC0")
	c.Assert(err, IsNil)
	c.Check(asset.String(), Equals, "BNB.SWIPE.B-DC0")
	c.Check(asset.Chain.Equals(BNBChain), Equals, true)
	c.Check(asset.Symbol.Equals(Symbol("SWIPE.B-DC0")), Equals, true)
	c.Check(asset.Ticker.Equals(Ticker("SWIPE.B")), Equals, true)

	// parse without chain
	asset, err = NewAsset("karma")
	c.Assert(err, IsNil)
	c.Check(asset.Equals(KarmaNative), Equals, true)

	// ETH test
	asset, err = NewAsset("eth.knc")
	c.Assert(err, IsNil)
	c.Check(asset.Chain.Equals(ETHChain), Equals, true)
	c.Check(asset.Symbol.Equals(Symbol("KNC")), Equals, true)
	c.Check(asset.Ticker.Equals(Ticker("KNC")), Equals, true)
	asset, err = NewAsset("ETH.KARMA-0x3155ba85d5f96b2d030a4966af206230e46849cb")
	c.Assert(err, IsNil)

	// DOGE test
	asset, err = NewAsset("doge.doge")
	c.Assert(err, IsNil)
	c.Check(asset.Chain.Equals(DOGEChain), Equals, true)
	c.Check(asset.Equals(DOGEAsset), Equals, true)
	c.Check(asset.IsKarma(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "DOGE.DOGE")

	// BCH test
	asset, err = NewAsset("bch.bch")
	c.Assert(err, IsNil)
	c.Check(asset.Chain.Equals(BCHChain), Equals, true)
	c.Check(asset.Equals(BCHAsset), Equals, true)
	c.Check(asset.IsKarma(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "BCH.BCH")

	// LTC test
	asset, err = NewAsset("ltc.ltc")
	c.Assert(err, IsNil)
	c.Check(asset.Chain.Equals(LTCChain), Equals, true)
	c.Check(asset.Equals(LTCAsset), Equals, true)
	c.Check(asset.IsKarma(), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "LTC.LTC")

	// btc/btc
	asset, err = NewAsset("btc/btc")
	c.Check(err, IsNil)
	c.Check(asset.Chain.Equals(BTCChain), Equals, true)
	c.Check(asset.Equals(BTCAsset), Equals, false)
	c.Check(asset.IsEmpty(), Equals, false)
	c.Check(asset.String(), Equals, "BTC/BTC")

	// test shorts
	asset, err = NewAssetWithShortCodes(semver.MustParse("1.115.0"), "b")
	c.Assert(err, IsNil)
	c.Check(asset.String(), Equals, "BTC.BTC")
	asset, err = NewAssetWithShortCodes(semver.MustParse("1.115.0"), "BLAH.BLAH")
	c.Assert(err, IsNil)
	c.Check(asset.String(), Equals, "BLAH.BLAH")

	asset, err = NewAssetWithShortCodes(semver.MustParse("0.0.0"), "BTC.BTC")
	c.Assert(err, IsNil)
	c.Check(asset.String(), Equals, "BTC.BTC")
}
