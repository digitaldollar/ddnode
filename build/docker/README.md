# DDNode Docker

## Fullnode

The default image will start a fullnode:

```bash
docker run \
  -e CHAIN_ID=ddchain-mainnet-v1 \
  -e NET=mainnet \
  registry.gitlab.com/digitaldollar/ddnode:mainnet
```

The above command will result in syncing chain state to ephemeral storage within the container, in order to persist data across restarts simply mount a local volume:

```bash
mkdir ddnode-data
docker run \
  -v $(pwd)/ddnode-data:/root/.ddnode \
  -e CHAIN_ID=ddchain-mainnet-v1 \
  -e NET=mainnet \
  registry.gitlab.com/digitaldollar/ddnode:mainnet
```

Nine Realms provides snapshots taken from a statesync recovery which can be rsync'd without need for a high memory (80G at time of writing) machine to recover the statesync snapshot. Ensure `gsutil` is installed, and pull the latest statesync snapshot via:

```bash
mkdir -p ddnode-data/data
HEIGHT=$(
  curl -s 'https://storage.googleapis.com/storage/v1/b/public-snapshots-ninerealms/o?delimiter=%2F&prefix=ddnode/pruned/' |
  jq -r '.prefixes | map(match("ddnode/pruned/([0-9]+)/").captures[0].string) | map(tonumber) | sort | reverse[0]'
)
gsutil -m rsync -r -d "gs://public-snapshots-ninerealms/ddnode/pruned/$HEIGHT/" ddnode-data/data
docker run \
  -v $(pwd)/ddnode-data:/root/.ddnode \
  -e CHAIN_ID=ddchain-mainnet-v1 \
  -e NET=mainnet \
  registry.gitlab.com/digitaldollar/ddnode:mainnet
```

Since this image tag contains the latest version of DDNode, the node can auto update by simply placing this in a loop to re-pull the image on exit:

```bash
while true; do
  docker pull registry.gitlab.com/digitaldollar/ddnode:mainnet
  docker run \
    -v $(pwd)/ddnode-data:/root/.ddnode \
    -e NET=mainnet \
    registry.gitlab.com/digitaldollar/ddnode:mainnet
do
```

The above commands also apply to `testnet` and `stagenet` by simply using the respective image (in these cases `-e NET=...` is not required):

```code
testnet  => registry.gitlab.com/digitaldollar/ddnode:testnet
stagenet => registry.gitlab.com/digitaldollar/ddnode:stagenet
```

## Validator

Officially supported deployments of DDNode validators require a working understanding of Kubernetes and related infrastructure. See the [Cluster Launcher](https://gitlab.com/digitaldollar/devops/cluster-launcher) repo for cluster Terraform resources, and the [Node Launcher](https://gitlab.com/digitaldollar/devops/node-launcher) repo for deployment utilities which internally leveraging Helm.

## Mocknet

The development environment leverages Docker Compose V2 to create a mock network - this is included in the latest version of Docker Desktop for Mac and Windows, and can be added as a plugin on Linux by following the instructions [here](https://docs.docker.com/compose/cli-command/#installing-compose-v2).

The mocknet configuration is vanilla, leveraging Docker Compose profiles which can be combined at user discretion. The following profiles exist:

```code
ddnode => ddnode only
bifrost  => bifrost and ddnode dependency
midgard  => midgard and ddnode dependency
mocknet  => all mocknet dependencies
```

### Keys

We leverage the following keys for testing and local mocknet setup, created with a simplified mnemonic for ease of reference. We refer to these keys by the name of the animal used:

```text
cat => cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat cat crawl
dog => dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog fossil
fox => fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox filter
pig => pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig pig quick
```

### Examples

Example commands are provided below for those less familiar with Docker Compose features:

```bash
# start a mocknet with all dependencies
docker compose --profile mocknet up -d

# multiple profiles are supported, start a mocknet and midgard
docker compose --profile mocknet --profile midgard up -d

# check running services
docker compose ps

# tail the logs of all services
docker compose logs -f

# tail the logs of only ddnode and bifrost
docker compose logs -f ddnode bifrost

# enter a shell in the ddnode container
docker compose exec ddnode sh

# copy a file from the ddnode container
docker compose cp ddnode:/root/.ddnode/config/genesis.json .

# rebuild all buildable services (ddnode and bifrost)
docker compose build

# export ddnode genesis
docker compose stop ddnode
docker compose run ddnode -- ddnode export
docker compose start ddnode

# hard fork ddnode
docker compose stop ddnode
docker compose run /docker/scripts/hard-fork.sh

# stop mocknet services
docker compose --profile mocknet down

# clear mocknet docker volumes
docker compose --profile mocknet down -v
```

## Multi-Node Mocknet

The Docker Compose configuration has been extended to support a multi-node local network. Starting the multinode network requires the `mocknet-cluster` profile:

```bash
docker compose --profile mocknet-cluster up -d
```

Once the mocknet is running, you can open open a shell in the `cli` service to access CLIs for interacting with the mocknet:

```bash
docker compose run cli

# increase default 60 block churn (keyring password is "password")
ddnode tx ddchain mimir CHURNINTERVAL 1000 --from dog $TX_FLAGS

# set limit to 1 new node per churn (keyring password is "password")
ddnode tx ddchain mimir NUMBEROFNEWNODESPERCHURN 1 --from dog $TX_FLAGS
```

## EVM Tool

The `evm-tool.py` script is leveraged during mocknet init in the `ddnode` container to create the router contract and test token, but may also be run directly for additional convenience targets.

### Create Gas and Token Pools

Note that the token address used in this command is the same as the address output in the logs during `ddnode` init (created by `evm-tool.py --action deploy`). Run the following within the `docker compose exec ddnode sh` shell to create the asset side of the pools:

```bash
python3 /scripts/evm/evm-tool.py --chain ETH --action deposit
python3 /scripts/evm/evm-tool.py --chain ETH --token-address 0x52C84043CD9c865236f11d9Fc9F56aa003c1f922 --action deposit-token
```

Run the following in the `docker compose run cli` shell to create the KARMA side of the pools:

```bash
ddnode tx ddchain deposit 10000000000 karma ADD:ETH.ETH:0x8db97c7cece249c2b98bdc0226cc4c2a57bf52fc --from cat $TX_FLAGS
ddnode tx ddchain deposit 10000000000 karma ADD:ETH.TKN-0X52C84043CD9C865236F11D9FC9F56AA003C1F922:0x8db97c7cece249c2b98bdc0226cc4c2a57bf52fc --from cat $TX_FLAGS
```

## Local Mainnet Fork of EVM Chain

There are scripts for creation of a mocknet using forked mainnet EVM chains for testing of aggregator contracts. See `[tools/evm/README.md](../../tools/evm/README.md)` for documentation.

## Bootstrap Mocknet Data

You can leverage the smoke tests to bootstrap local vaults with a subset of test data. Run:

```bash
make bootstrap-mocknet
```
