#!/bin/sh

set -o pipefail

export SIGNER_NAME="${SIGNER_NAME:=ddchain}"
export SIGNER_PASSWD="${SIGNER_PASSWD:=password}"

. "$(dirname "$0")/core.sh"

if [ ! -f ~/.ddnode/config/genesis.json ]; then
  init_chain
  rm -rf ~/.ddnode/config/genesis.json # set in ddnode render-config
fi

# render tendermint and cosmos configuration files
ddnode render-config

exec ddnode start
