#!/bin/sh

# https://docs.docker.com/compose/startup-order/

set -e

echo "Waiting for DDChain API..."

until curl -s "$1/ddchain/ping" >/dev/null; do
  # echo "Rest server is unavailable - sleeping"
  sleep 1
done

echo "DDChain API ready"
