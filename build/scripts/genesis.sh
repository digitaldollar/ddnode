#!/bin/sh

set -o pipefail

. "$(dirname "$0")/core.sh"

if [ "$NET" = "mocknet" ] || [ "$NET" = "testnet" ]; then
  echo "Loading unsafe init for mocknet and testnet..."
  . "$(dirname "$0")/core-unsafe.sh"
  . "$(dirname "$0")/testnet/state.sh"
fi

NODES="${NODES:=1}"
SEED="${SEED:=ddnode}" # the hostname of the master node
DD_BLOCK_TIME="${DD_BLOCK_TIME:=5s}"
CHAIN_ID=${CHAIN_ID:=ddchain}

# this is required as it need to run ddnode init, otherwise tendermint related command doesn't work
if [ "$SEED" = "$(hostname)" ]; then
  if [ ! -f ~/.ddnode/config/priv_validator_key.json ]; then
    init_chain
    # remove the original generate genesis file, as below will init chain again
    rm -rf ~/.ddnode/config/genesis.json
  fi
fi

create_dd_user "$SIGNER_NAME" "$SIGNER_PASSWD" "$SIGNER_SEED_PHRASE"

VALIDATOR=$(ddnode tendermint show-validator | ddnode pubkey --bech cons)
NODE_ADDRESS=$(echo "$SIGNER_PASSWD" | ddnode keys show ddchain -a --keyring-backend file)
NODE_PUB_KEY=$(echo "$SIGNER_PASSWD" | ddnode keys show ddchain -p --keyring-backend file | ddnode pubkey)
VERSION=$(fetch_version)

if [ "$SEED" = "$(hostname)" ]; then
  echo "Setting DDNode as genesis"
  if [ ! -f ~/.ddnode/config/genesis.json ]; then
    # add ourselves to the genesis state
    NODE_IP_ADDRESS=${EXTERNAL_IP:=$(curl -s http://whatismyip.akamai.com)}

    init_chain "$NODE_ADDRESS"
    add_node_account "$NODE_ADDRESS" "$VALIDATOR" "$NODE_PUB_KEY" "$VERSION" "$NODE_ADDRESS" "$NODE_PUB_KEY_ED25519" "$NODE_IP_ADDRESS"

    # disable default bank transfer, and opt to use our own custom one
    disable_bank_send

    # for mocknet, add initial balances
    echo "Using NET $NET"
    if [ "$NET" = "mocknet" ]; then
      echo "Setting up accounts"

      # local cluster accounts (2M KARMA)
      add_account dd1uuds8pd92qnnq0udw0rpg0szpgcslc9pclc7xx karma 200000000000000 # cat
      add_account dd1zf3gsk7edzwl9syyefvfhle37cjtql35g639t0 karma 200000000000000 # dog
      add_account dd13wrmhnh2qe98rjse30pl7u6jxszjjwl4k6rx8h karma 200000000000000 # fox
      add_account dd1qk8c8sfrmfm0tkncs0zxeutc8v5mx3pjd0e52g karma 200000000000000 # pig

      reserve 22000000000000000

      # deploy evm contracts
      deploy_evm_contracts
    else
      if [ -n "${ETH_CONTRACT+x}" ]; then
        echo "ETH Contract Address: $ETH_CONTRACT"
        set_eth_contract "$ETH_CONTRACT"
      fi
      # if [ -n "${AVAX_CONTRACT+x}" ]; then
      #   echo "AVAX Contract Address: $AVAX_CONTRACT"
      #   set_avax_contract "$AVAX_CONTRACT"
      # fi
      if [ -n "${BSC_CONTRACT+x}" ]; then
        echo "BSC Contract Address: $BSC_CONTRACT"
        set_bsc_contract "$BSC_CONTRACT"
      fi
    fi

    # if [ "$NET" = "testnet" ]; then
    #   # mint 1m KARMA to reserve for testnet
    #   reserve 100000000000000

    #   # add testnet account and balances
    #   # testnet_add_accounts
    # elif [ "$NET" = "stagenet" ]; then
    #   if [ -z ${FAUCET+x} ]; then
    #     echo "env variable 'FAUCET' is not defined: should be a sdd address"
    #     exit 1
    #   fi
    #   add_account "$FAUCET" 50000000000000000 karma
    # fi

    echo "Genesis content"
    cat ~/.ddnode/config/genesis.json
    ddnode validate-genesis --trace
  fi
fi

# setup peer connection, typically only used for some mocknet configurations
if [ "$SEED" != "$(hostname)" ]; then
  if [ ! -f ~/.ddnode/config/genesis.json ]; then
    echo "Setting DDNode as peer not genesis"

    init_chain "$NODE_ADDRESS"
    NODE_ID=$(fetch_node_id "$SEED")
    echo "NODE ID: $NODE_ID"
    export DD_TENDERMINT_P2P_PERSISTENT_PEERS="$NODE_ID@$SEED:$PORT_P2P"

    cat ~/.ddnode/config/genesis.json
  fi
fi

# render tendermint and cosmos configuration files
ddnode render-config

export SIGNER_NAME
export SIGNER_PASSWD
cp -r "$(dirname "$0")/config.toml"  ~/.ddnode/config/
exec "$@"
