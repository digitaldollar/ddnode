#!/bin/sh

set -o pipefail

add_account() {
  jq --arg ADDRESS "$1" --arg ASSET "$2" --arg AMOUNT "$3" '.app_state.auth.accounts += [{
        "@type": "/cosmos.auth.v1beta1.BaseAccount",
        "address": $ADDRESS,
        "pub_key": null,
        "account_number": "0",
        "sequence": "0"
    }]' <~/.ddnode/config/genesis.json >/tmp/genesis.json
  mv /tmp/genesis.json ~/.ddnode/config/genesis.json

  jq --arg ADDRESS "$1" --arg ASSET "$2" --arg AMOUNT "$3" '.app_state.bank.balances += [{
        "address": $ADDRESS,
        "coins": [ { "denom": $ASSET, "amount": $AMOUNT } ],
    }]' <~/.ddnode/config/genesis.json >/tmp/genesis.json
  mv /tmp/genesis.json ~/.ddnode/config/genesis.json
}

deploy_evm_contracts() {
  for CHAIN in ETH BSC; do
    # deploy contract and get address from output
    echo "Deploying $CHAIN contracts"
    if ! python3 scripts/evm/evm-tool.py --chain $CHAIN --rpc "$(eval echo "\$${CHAIN}_HOST")" --action deploy >/tmp/evm-tool.log 2>&1; then
      cat /tmp/evm-tool.log && exit 1
    fi
    cat /tmp/evm-tool.log
    CONTRACT=$(grep </tmp/evm-tool.log "Router Contract Address" | awk '{print $NF}')

    # add contract address to genesis
    echo "$CHAIN Contract Address: $CONTRACT"
    jq --arg CHAIN "$CHAIN" --arg CONTRACT "$CONTRACT" \
      '.app_state.ddchain.chain_contracts += [{"chain": $CHAIN, "router": $CONTRACT}]' \
      ~/.ddnode/config/genesis.json >/tmp/genesis.json
    mv /tmp/genesis.json ~/.ddnode/config/genesis.json
  done
}

set_eth_contract() {
  jq --arg CONTRACT "$1" '.app_state.ddchain.chain_contracts = [{"chain": "ETH", "address": $CONTRACT}]' ~/.ddd/config/genesis.json >/tmp/genesis.json
  mv /tmp/genesis.json ~/.ddd/config/genesis.json
}

set_avax_contract() {
  jq --arg CONTRACT "$1" '.app_state.ddchain.chain_contracts = [{"chain": "AVAX", "address": $CONTRACT}]' ~/.ddd/config/genesis.json >/tmp/genesis.json
  mv /tmp/genesis.json ~/.ddd/config/genesis.json
}

set_bsc_contract() {
  jq --arg CONTRACT "$1" '.app_state.ddchain.chain_contracts = [{"chain": "BSC", "address": $CONTRACT}]' ~/.ddd/config/genesis.json >/tmp/genesis.json
  mv /tmp/genesis.json ~/.ddd/config/genesis.json
}

gen_bnb_address() {
  if [ ! -f ~/.bond/private_key.txt ]; then
    echo "Generating BNB address"
    mkdir -p ~/.bond
    # because the generate command can get API rate limited, DDNode may need to retry
    n=0
    until [ $n -ge 60 ]; do
      generate >/tmp/bnb && break
      n=$((n + 1))
      sleep 1
    done
    ADDRESS=$(grep </tmp/bnb MASTER= | awk -F= '{print $NF}')
    echo "$ADDRESS" >~/.bond/address.txt
    BINANCE_PRIVATE_KEY=$(grep </tmp/bnb MASTER_KEY= | awk -F= '{print $NF}')
    echo "$BINANCE_PRIVATE_KEY" >/root/.bond/private_key.txt
    PUBKEY=$(grep </tmp/bnb MASTER_PUBKEY= | awk -F= '{print $NF}')
    echo "$PUBKEY" >/root/.bond/pubkey.txt
    MNEMONIC=$(grep </tmp/bnb MASTER_MNEMONIC= | awk -F= '{print $NF}')
    echo "$MNEMONIC" >/root/.bond/mnemonic.txt
  fi
}
